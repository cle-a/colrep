function [sqc] = COLREP_fx_makeblocks(sqc)

%%% making blocks:
%%% this runs on preconstituted sequences, splits them into blocks
%%% the sequences within blocs are ordered afterwards with fx_orderblocks
%%%     - make random draw of 12 seq:
%%%     - check identical nb of ('condition' x 'symmetry') within block
%%%     - check same amount of blue/orange targets in each condition (1 and 2)
%%%     - check not over 7 times same symbol (noksymrepeats) within block
%%%     - check not over 2 times same pair (nuniquepairs) within block

NSEQ            = numel(sqc);
nblocks         = NSEQ/16; % blocks of 16 sequences (~ 7-10minutes)
nseqblock       = NSEQ/nblocks;
nseqcond        = nseqblock/2/4;  % 8 combinations of condition (=2) x symmetry (=4: samegood/samebad/diffgood/diffbad) = 2 of each
nseqcol         = nseqblock/2/2;  % 4 of each condition x color (16 seq / 2 conditions / 2 colors = 4)
noksymrepeats   = 7;
nokpairsrepeats = 3;
nuniquepairs    = nseqblock - nokpairsrepeats;

fprintf('Making blocks of seqs from >>>fx_makeblocks:\n');
while true
    
    %%% init temp structure with blk field:
    currentpool = sqc;
    temp        = sqc;
    temp(:)     = [];
    [temp.blk]  = [];
    
    %%% loop on blocks:
    for iblock = 1:nblocks
        while true
            % init drawsequences:
            idrawnseq = randperm(numel(currentpool),nseqblock);
            drawnseq  = currentpool(idrawnseq);
            
            % check constraints: 
            c1a1 = sum([drawnseq.condition] == 1 & [drawnseq.symmetry] == 1);
            c1a2 = sum([drawnseq.condition] == 1 & [drawnseq.symmetry] == 2);
            c1a3 = sum([drawnseq.condition] == 1 & [drawnseq.symmetry] == 3);
            
            c2a1 = sum([drawnseq.condition] == 2 & [drawnseq.symmetry] == 1);
            c2a2 = sum([drawnseq.condition] == 2 & [drawnseq.symmetry] == 2);
            c2a3 = sum([drawnseq.condition] == 2 & [drawnseq.symmetry] == 3);
            
            maxsymreps = max(histcounts([drawnseq.symbolpair]));     % get max nb of occurrences of same symbol
            pairs = reshape([drawnseq.symbolpair], [2, nseqblock])'; % get pairs from drawnseqs
            drawnpairs = size(unique(pairs,'rows'),1);               % count unique pairs
            
            ncoltarg   = sum([drawnseq.condition] == 1 & [drawnseq.targetcolor] == 2); % check nb of same color per condition
            ncolopen   = sum([drawnseq.condition] == 2 & [drawnseq.targetcolor] == 2);
            nprobeside = sum([drawnseq.probeside] == 1);
            
            % if matches the constrains, break out:
            if  c1a1 == nseqcond && c1a2 == nseqcond && c1a3 == nseqcond*2 ...
             && c2a1 == nseqcond && c2a2 == nseqcond && c2a3 == nseqcond*2 ...
             && maxsymreps < noksymrepeats && drawnpairs >= nuniquepairs ...
             && ncoltarg == nseqcol && ncolopen == nseqcol ...
             && nprobeside == nseqblock / 2
                fprintf('...done block %d\n', iblock);
                break
            elseif iblock == nblocks && (( maxsymreps >= noksymrepeats) || (drawnpairs < nuniquepairs)) % if at 6th block, too many of same symbol, restart at block1
                currentpool = sqc;
                iblock = 1; % go back to block1
                fprintf('...!!! BREAKING AT LAST BLOCK !!!\n')
                break
            end % if checking conditions for this block: either break or redo this block
        end % while this block is not good enough: redo it
        
        % store drawn seqs, remove them from pool of seqs
        [drawnseq.blk] = deal(iblock);
        temp = [temp drawnseq];
        currentpool(idrawnseq) = [];
        
    end % looping over blocks
    
    %%% check all blocks succeeded:  
    if isempty(currentpool)
        break
    else  
    end % if all blocks succeeded, else restart from block 1
    
end % while havent found all blocks, restart at block 1

sqc = temp;

fprintf('Done making blocks!\n');


end% function definition
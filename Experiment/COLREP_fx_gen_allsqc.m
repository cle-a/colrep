%%% generates the sqc structures for n participants
% check if dir exists first, if not, writes it to MAIN/DATA/S_ni
% if exists, will crash.
% will take ~ 30min per participant, depending on the nb of sqc

for subj = 1:20
    ti = GetSecs; 
    
    fprintf('>>>Creating sequences for subj # %02d\n', subj)
    
    subjdir = sprintf('../Data/S%02d', subj);
    if ~exist(subjdir,'dir')
        mkdir(subjdir);
    elseif exist(subjdir,'dir') % if dir already exists: crash the whole thing -> check.
        error('there are already subj data in the target directories: check Data/')
    end
    
    sqc = COLREP_fx_gen_expe_sqc(subj, 128, 1/3);
    save(sprintf('%s/S%02d_sqc.mat', subjdir, subj), 'sqc');
    
    to = GetSecs;
    fprintf('took %.f min \n', (to-ti)/60)
end


%%% running the whole COLREP experiment from runner:

close all
clear all java
close all hidden
clc
addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path

testing = 0;

subj = input('>>>  Input participant nb: ');
if ~isscalar(subj) || mod(subj,1) ~= 0
    error('Invalid subject number!');
end
subjdir = sprintf('../Data/S%02d',subj);

diary(sprintf('../Data/S%02d/S%02d_log_%s.txt', subj, subj, datestr(now,'mmdd_HHMM')));

fprintf('Saving into %s\n', subjdir);

% run tutorial:
fprintf('...Launching tutorial now \n');
tutorialoutput = COLREP_fx_run_tutorial(testing);
save(sprintf('%s/S%02d_r_tutorial.mat', subjdir, subj), 'tutorialoutput');% 
fprintf('...Finished tutorial, went from %s to %s ... \n', tutorialoutput.start, tutorialoutput.end);
fprintf('        waiting for [t] press... ... \n');
WaitKeyPress(KbName('t'));

% run training:
fprintf('...Launching training now \n');
load('training_sqc.mat')
trainingoutput = COLREP_fx_run_expe(training_sqc, subj, 'training', testing);
save(sprintf('%s/S%02d_r_training.mat', subjdir, subj), 'trainingoutput');
fprintf('...Finished training, went from %s to %s ... \n', trainingoutput.header.start, trainingoutput.header.end);
fprintf('        waiting for [t] press... ... \n');
WaitKeyPress(KbName('t'));

% run experiment
fprintf('...Loading existing runs from ''%s/S%02d_sqc.mat''...\n', subjdir, subj)
load(sprintf('%s/S%02d_sqc.mat', subjdir, subj),'sqc');
fprintf('*****\n*****\n...Launching experiment now... \n');
expeoutput = COLREP_fx_run_expe(sqc, subj, 'expe', testing);
save(sprintf('%s/S%02d_r_expe.mat', subjdir, subj), 'expeoutput');
fprintf('FINISHED EXPERIMENT, went from %s to %s ... \n', expeoutput.header.start, expeoutput.header.end);
diary off;


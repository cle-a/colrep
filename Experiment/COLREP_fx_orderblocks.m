function [sqc] = COLREP_fx_orderblocks(sqc)

%%% ordering sequences within blocks
%%% this runs on preconstituted sequences and blocks made from COLEXP_expe_makeblocks
%%%     - shuffle drawn sequences
%%%     - checking not twice same condition or 'symmetry' in a row
%%%     - check not twice the same symbol in a row: nbackcondition

NSEQ = numel(sqc);
nblocks = max(unique([sqc.blk]));
nseqblk = NSEQ/nblocks;
nback = 2;

%init
temp = sqc;
temp(:) = [];
[temp.blkseq] = [];

fprintf('---> Ordering sequences within the blocks from >>> fx_orderblocks:\n');
for iblock = 1:nblocks
    
    %get sequences of this block
    fprintf('...ordering block %d --', iblock);
    thisblock = sqc([sqc.blk] == iblock);
    
    %while shuffling doesn't match constraints, reshuffle
    while true
        
        %shuffle sequences of this block
        shuffle = randperm(numel(thisblock),numel(thisblock));
        thisblock = thisblock(shuffle);
        
        %for each seq check constraints
        thisblock(1).blkseq = 1;
        for rnk = 2:nseqblk
            
            % (1) check 2 back for condition and symmetry: doesn't repeat more often than every 3 times in a row.
            if rnk == 2
                diffcond = 1; 
                diffsymm = 1;
                diffprob = 1;
            else
                diffcond = sum(abs(diff([thisblock(rnk-nback:rnk).condition])));
                diffsymm = sum(abs(diff([thisblock(rnk-nback:rnk).symmetry])));
                if diffcond == 1
                    diffprob = sum(abs(diff([thisblock(rnk-nback:rnk).probeside])));
                else
                    diffprob = 0;
                end
            end
            
            % (2) for condition 1, symmetry badbad: cannot happen twice in a row.
            if thisblock(rnk).condition == 1 && thisblock(rnk).symmetry == 2 
                diffbadbad = ~ (thisblock(rnk-1).symmetry == thisblock(rnk).symmetry);
            else; diffbadbad = 1;
            end
              
            % (3) symbols cannot repeat twice in a row.
            symlast = sum(ismember(thisblock(rnk-1).symbolpair, thisblock(rnk).symbolpair));
            
            % => if conditions are not met, break and restart looping over seqs for this block:
            if symlast ~= 0 || diffcond == 0 || diffsymm == 0 || diffbadbad == 0
                break
            end
            
            thisblock(rnk).blkseq = rnk;
        end
                
        %if successfully checked all seqs = block is done = break
        if rnk == nseqblk && symlast == 0 && diffcond ~= 0 && diffsymm ~=0 && diffbadbad ~=0 && diffprob ~= 0
            break
        end
        
    end %while this block is not good enough, shuffle again
    
    % once good enough, store
    temp = [temp thisblock];
    fprintf('...ok %d\n', iblock);
    
end%FOR each block

sqc = temp;

fprintf('Done ordering blocks!\n');

end% function definition
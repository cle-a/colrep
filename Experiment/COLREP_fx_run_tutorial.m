%%% run tutorial sequences
%%% sequences are hard coded in here because they were the same for all participants, like training
%%%
%%% NB: native KB keys are not inhibited: don't press a | p keys!
%%% NB: THIS IS NOT SAVING ANYTHING

function header = COLREP_fx_run_tutorial(testing)

try
    %% setup PTB :
    addpath ./Toolboxes/IO % Valentin's toolbox in Experiment's path
    
    % parameters:
    video.testingON = testing;
    video.synchflip = true;
    ntrials = 7;
    
    if testing == 1
        showtime = 0.5;
    elseif testing == 0
        showtime = 2;
        HideCursor;
        FlushEvents;
        ListenChar(2);
    end
    header = struct;
    header.err = string;
    
    %%% PTB parameters:
    %%% Set visual parameters
    video.eyelink.inputphys_mm  = [660 337]; %downstairs
    video.eyelink.inputscreendistance_mm = 550;
    if ispc % in 2nd floor booths/downstairs
        video.screenwdth_cm = 66;
        video.screenwdth_px = 1920;
        video.ppd = round(tand(1) * video.eyelink.inputscreendistance_mm / 10/ video.screenwdth_cm * video.screenwdth_px);
        %  where:
        %    dist_cm - screen-eye distance in centimeters
        %    wdth_cm - screen width in centimeters
        %    wdth_px - screen width in pixels (native resolution)
    elseif ismac % testing non mac, doesnt care.
        video.ppd = 40; % number of screen pixels per degree of visual angle
    end
    
    video.bag_sz            = 7*video.ppd;
    video.shape_sz          = 5*video.ppd;  % video.ppd pixels per degree of visual angle = 40
    video.shape_offset      = 6*video.ppd;  % shape offset
    video.respbutton_offset = 12*video.ppd; % response button offset
    video.rep_sz            = 5*video.ppd;
    video.fb_sz             = 6*video.ppd;  % selection square size
    video.cup_sz            = 6*video.ppd;
    video.black             = [0,0,0];
    white                   = [1,1,1];
    video.lbgd              = 0.3950;
    orange                  = [1,0.5,0];
    blue                    = [0,0.5,1];
    
    %%% which screens?
    screens = Screen('Screens');
    video.screen = max(screens); %screen index on PC: 0=extended, 1=native, 2=ext
    
    %%% set synch properties:
    if video.synchflip && ispc
        % set screen synchronization properties -- workaround Valentin for PC
        % see 'help SyncTrouble',
        %     'help BeampositionQueries' or
        %     'help ConserveVRAMSettings' for more information
        Screen('Preference','VisualDebuglevel',3); % verbosity
        Screen('Preference','SyncTestSettings',[],[],0.2,10); % soften synchronization test requirements
        Screen('Preference','ConserveVRAM',bitor(4096,Screen('Preference','ConserveVRAM'))); % enforce beamposition workaround for missing VBL interval
        fprintf('Synching flips with softer requirements...\n');
    elseif ~video.synchflip || ismac
        % skip synchronization tests altogether
        % //!\\ force skipping tests, PTB wont work = timing inaccurate
        Screen('Preference','SkipSyncTests',2); % assumes 60Hz etc..
        Screen('Preference','VisualDebuglevel',0);
        Screen('Preference','SuppressAllWarnings',1);
        fprintf('||| SYNCHFLIP OFF or running on OSX => TIMINGS WILL BE INACCURATE! |||\n')
    end
    
    %%% open main window:
    PsychImaging('PrepareConfiguration');
    PsychImaging('AddTask','General','UseFastOffscreenWindows');
    PsychImaging('AddTask','General','NormalizedHighresColorRange');
    video.res = Screen('Resolution',video.screen);
    [video.window, video.windowRect] = PsychImaging('OpenWindow',video.screen,video.lbgd);
    fprintf('passed OpenWindow!\n')
    
    [video.screenXpixels, video.screenYpixels] = Screen('WindowSize', video.window);
    [video.xCenter, video.yCenter] = RectCenter(video.windowRect);
    Screen('BlendFunction', video.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');%smooth blending/antialiasing
    video.blend = '''GL_SRC_ALPHA'', ''GL_ONE_MINUS_SRC_ALPHA''';
    video.colorrange = 1;
    Screen('ColorRange',video.window,video.colorrange);
    % 1 = to pass color values in OpenGL's native floating point color range of 0.0 to
    % 1.0: This has two advantages: First, your color values are independent of
    % display device depth, i.e. no need to rewrite your code when running it on
    % higher resolution hardware. Second, PTB can skip any color range remapping
    % operations - this can speed up drawing significantly in some cases.   ...???
    video.textsize = ceil(video.windowRect(4)*0.035);
    Screen('TextSize', video.window, video.textsize);
    video.ifi = Screen('GetFlipInterval', video.window);
    video.priority = Priority(MaxPriority(video.window));
    Screen('Flip', video.window);
    
    %% Make textures:
    
    % bag textures:
    for ibag = 1:2 % 1 = inside 2 = outline
        bagimg      = double(imread(sprintf('./img/bag%d.png', ibag)));
        bagimg      = imresize(bagimg, video.bag_sz/size(bagimg, 1));
        bagtx(ibag) = Screen('MakeTexture', video.window, cat(3, ones(size(bagimg)), bagimg), [], [], 2);
    end
    
    % shape textures:
    shapetx = zeros(2,8); %dim1: 1=inside / 2 =outline  // dim2: shape nb
    for i = 1:2
        for sh = 1:8
            shapeimg      = double(imread(sprintf('./img/shape%d_%d.png', sh, i)))/255;
            shapeimg      = imresize(shapeimg,video.shape_sz/size(shapeimg,1));
            shapetx(i,sh) = Screen('MakeTexture', video.window, cat(3,ones(size(shapeimg)), shapeimg), [],[], 2);
        end
    end
    
    % define position rectangles of textures: positions for the sampling probes:
    video.dotx     = [video.xCenter]; video.doty = [video.screenYpixels*0.55];
    bagpos(1,:)    = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter-video.shape_offset, video.yCenter);
    bagpos(2,:)    = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter+video.shape_offset, video.yCenter);
    bagheight      = (bagpos(1,3)-bagpos(1,1)); % centering shapes on bags
    bagwidth       = (bagpos(1,4)-bagpos(1,2));
    shapepos(1,:)  = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter - video.shape_offset, video.yCenter + bagheight* 0.2);
    shapepos(2,:)  = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter + video.shape_offset, video.yCenter + bagheight * 0.2);
    
    % define positions for the final probe
    respbagpos(1,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter- video.respbutton_offset, video.screenYpixels*0.65);
    respbagpos(2,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter+ video.respbutton_offset, video.screenYpixels*0.65);
    respbagpos(3,:) = CenterRectOnPoint(Screen('Rect', bagtx(2)), video.xCenter, video.screenYpixels*0.35);
    
    respshapepos(1,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter- video.respbutton_offset, video.screenYpixels*0.65 + bagheight* 0.2);
    respshapepos(2,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter+ video.respbutton_offset, video.screenYpixels*0.65 + bagheight* 0.2);
    respshapepos(3,:) = CenterRectOnPoint(Screen('Rect',shapetx(1, 1)), video.xCenter, video.screenYpixels*0.35 + bagheight* 0.2);
    
    buttonbuffer        = 0.1*bagwidth;
    selsize             = [0 0 bagwidth+buttonbuffer bagwidth+buttonbuffer];
    selection_rect(1,:) = CenterRectOnPoint(selsize, video.xCenter-video.respbutton_offset,video.screenYpixels*0.65);
    selection_rect(2,:) = CenterRectOnPoint(selsize, video.xCenter+video.respbutton_offset,video.screenYpixels*0.65);
    
    % score feedback: cup textures:
    cupi         = imread('./img/cupimg.png');
    cupi         = imresize(cupi,video.shape_sz/size(cupi,1));
    cuptext      = Screen('MakeTexture', video.window, cat(3,ones(size(cupi)), cupi));
    cuppos(1,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter-(video.shape_sz),video.yCenter);
    cuppos(2,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter, video.yCenter);
    cuppos(3,:)  = CenterRectOnPoint(Screen('Rect',cuptext),video.xCenter+(video.shape_sz),video.yCenter);
    
    buffer        = 0.3*size(cupi,2);
    cupframe      = [0 0 3*size(cupi,2)+buffer size(cupi,1)+buffer];
    cupframe_rect = CenterRectOnPoint(cupframe, video.xCenter,video.yCenter);
    
    %% Initialise response keys:
    
    clear PsychHID; % Force new enumeration of devices.
    clear KbCheck;
    KbName('UnifyKeyNames'); %across OSs
    GetKeyboardIndices();
    
    escapeKey   = KbName('ESCAPE'); %experimenter keys (native Kb)
    continueKey = KbName('y');
    
    spaceKey    = KbName('space'); %participant keys (numpad)
    if ispc; leftKey = KbName('a');
    elseif ismac; leftKey = KbName('q');
    end
    rightKey    = KbName('p');
    
    respKeys     = [leftKey, rightKey];% left=1 right=2
    expeKeys     = [escapeKey, continueKey, leftKey, rightKey, spaceKey];
    allKeys      = [];
    k = RestrictKeysForKbCheck(allKeys);
    
    %% Make tutorial sequences:
    tutorialcolors = [...
        0.4450    0.8030    0.5630    ; ...
        0.5190    0.6830    0.9250    ;...
        0.7110    0.4450    0.5320    ;...
        0.8190   -0.3110   -0.1670    ;...
        0.9910    0.7250    0.8890    ;...
        0.3630   -0.1150   -0.0370    ;...
        0.4490    0.4030   -0.2320    ];
        
    tutorialsqc(1).condition    = NaN;
    tutorialsqc(1).symmetry     = 3; % symbols are counterfactual
    tutorialsqc(1).symbols      = [5, 6];
    tutorialsqc(1).targetsymbol = 5; %star
    tutorialsqc(1).othersymbol  = 6; %cross
    tutorialsqc(1).symbolsides  = [1 1 1 1 2 2 2];
    tutorialsqc(1).colorslin   = [tutorialcolors(:,1) (-1) * tutorialcolors(:,1)]';
    
    tutorialsqc(2).condition    = 1; % target condition
    tutorialsqc(2).targetcolor  = 2; % target is blue
    tutorialsqc(2).symmetry     = 3; % symbols are counterfactual
    tutorialsqc(2).symbols      = [8, 7]; 
    tutorialsqc(2).targetsymbol = 8; %moon
    tutorialsqc(2).othersymbol  = 7; %square
    tutorialsqc(2).symbolsides  = [1 2 1 1 1 2 2];
    tutorialsqc(2).colorslin   = [(-1) * tutorialcolors(:,2) tutorialcolors(:,2)]'; %target is blue & they're counterfactual
    
    tutorialsqc(3).condition    = 2; % condition 2
    tutorialsqc(3).targetcolor  = 1; % target is orange
    tutorialsqc(3).symmetry     = 3; % symbols are counterfactual
    tutorialsqc(3).symbols      = [1, 6];
    tutorialsqc(3).targetsymbol = 1; %circle
    tutorialsqc(3).othersymbol  = 6; %cross
    tutorialsqc(3).symbolsides  = [1 1 1 2 2 1 2];
    tutorialsqc(3).colorslin   = [tutorialcolors(:,3) (-1) * tutorialcolors(:,3)]'; % target is orange & they're counterfactual:
    
    %% Test kb, hide cursor, stop spilling keys, display welcome screen:
    if ~video.testingON
        fprintf('Press [ESPACE] to check keyboard responsiveness...: ');
        if WaitKeyPress([],30) == 0
            fprintf('\n\n');
            error('No key press detected after 30 seconds.');
        else;fprintf('Good.\n\n');
        end
        HideCursor;
        FlushEvents;
        ListenChar(2);
    end %setting testing parameters
    
    header.start = datestr(now,'yyyymmdd-HHMM');
    
    welcome = sprintf('Voici les instructions pour cette exp\0351rience.\nLisez attentivement, vous devrez faire un petit r\0351sum\0351 de ce que vous avez compris :)\n\n\n');
    DrawFormattedText(video.window, welcome, 'center', video.screenYpixels * 0.25, video.black);
    spaceline = '[espace]';
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);
    Screen('Flip', video.window);
    WaitKeyPress(spaceKey);
    
    %% INSTRUCTIONS: sacs et tirages :
    
    %%% Dans cette tâche, vous jouerez avec deux sacs de formes + dégradé des couleurs:
    top = [sprintf('Dans cette t\0342che, vous allez jouer avec deux sacs contenant des formes de couleur,\n entre le orange et le bleu.\n\n') ...
            sprintf('Chaque sac contient soit des formes plut\0364t bleues,\nsoit des formes plut\0364t oranges :\n\n')];
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.10, video.black);
    
    targetsymbol = tutorialsqc(1).targetsymbol; % = 5 = star
    othersymbol = tutorialsqc(1).othersymbol;   % = 6 = cross
    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(1,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(1,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(1,:), 0, [], [], video.lbgd);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(2,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(2,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(2,:), 0, [], [], video.lbgd);
    
    spaceline = sprintf('[espace]');
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);
    
    continuum     = 'tutorial_continuum.jpg';
    contimg       = imread(continuum);
    conttex  = Screen('MakeTexture', video.window, contimg);
    contrect = [0 0 video.screenXpixels*0.7 video.screenYpixels*0.05];
    contpos  = CenterRectOnPointd(contrect, video.screenXpixels*0.5, video.screenYpixels*0.75);
    Screen('DrawTexture', video.window, conttex, [], contpos);
    Screen('FrameRect', video.window, [0 0 0], contpos, 3);
    
    Screen('Flip', video.window);% Flip and wait...
    WaitSecs(2);
    WaitKeyPress(spaceKey);
    
    %%% Tirer avec les touches [a] et [p]:
    [~, ~, asize] = DrawFormattedText(video.window, '[a]', [], [] * 0.75, white);
    [~, ~, psize] = DrawFormattedText(video.window, '[p]', [], [] * 0.75, white);
    asize = asize(3) - asize(1);
    psize = psize(3) - psize(1);
    Screen('FillRect', video.window, video.lbgd);
    targetsymbol = tutorialsqc(1).targetsymbol; % = 5 = star
    othersymbol = tutorialsqc(1).othersymbol;   % = 6 = cross
    
    for itrial = 1:ntrials
        
        % show sampling alternative
        if itrial == 1
            top = [sprintf('\0300 chaque s\0351quence, l''ordinateur vous propose deux nouveaux sacs de formes,\n')...
                   sprintf('avec les touches [a] et [p], vous pouvez piocher une forme dans un des sacs,\n')...
                   sprintf('dont la couleur sera plus ou moins franche.\n\n')...
                   sprintf('Vous pouvez piocher plusieurs fois, jusqu''\0340 ce que la s\0351quence s''arr\0352te (au hasard).\n')];
            DrawFormattedText(video.window, top, video.screenXpixels * 0.1, video.screenYpixels * 0.10, video.black);
        end
        bottom = sprintf('Piochez une forme !');
        DrawFormattedText(video.window, bottom, 'center', video.screenYpixels * 0.90, video.black);

        DrawFormattedText(video.window, '[a]', video.xCenter - video.shape_offset - asize/2, video.screenYpixels * 0.75, white);
        DrawFormattedText(video.window, '[p]', video.xCenter + video.shape_offset - psize/2, video.screenYpixels * 0.75, white);
        
        dotrial(1, itrial)
    end
    
    fin = sprintf('Fin de la s\0351quence !');
    DrawFormattedText(video.window, fin, 'center', 'center', video.black);
    Screen('Flip', video.window);
    WaitSecs(1);
    
    %% INSTRUCTIONS: conditions :

    top = sprintf('Il y aura deux types de consignes pour les s\0351quences :');
    DrawFormattedText(video.window, top, video.screenXpixels * 0.1, video.screenYpixels * 0.20, video.black);
    
    cond1     = 'tutorial_cond1.jpg';
    cond1img  = imread(cond1);
    cond1tex  = Screen('MakeTexture', video.window, cond1img);
    cond1rect = [0 0 video.screenXpixels*0.40 video.screenYpixels*0.40];
    cond1pos  = CenterRectOnPointd(cond1rect, video.screenXpixels*0.25, video.screenYpixels*0.60);
    Screen('DrawTexture', video.window, cond1tex, [], cond1pos);
    Screen('FrameRect', video.window, [0 0 0], cond1pos, 3);
    left      = sprintf('soit vous devrez piocher\nun maximum de formes\nde la bonne couleur (oranges ou bleues) :');
    DrawFormattedText(video.window, left, video.screenXpixels * 0.25 - cond1rect(3)/2, video.screenYpixels * 0.30, video.black);
    
    cond2     = 'tutorial_cond2.jpg';
    cond2img  = imread(cond2);
    cond2tex  = Screen('MakeTexture', video.window, cond2img);
    cond2rect = [0 0 video.screenXpixels*0.40 video.screenYpixels*0.40];
    cond2pos  = CenterRectOnPointd(cond2rect, video.screenXpixels*0.75, video.screenYpixels*0.60);
    Screen('DrawTexture', video.window, cond2tex, [], cond2pos);
    Screen('FrameRect', video.window, [0 0 0], cond2pos, 3);
    right     = sprintf('soit vous devrez piocher\npour deviner la couleur\nde chaque sac :');
    DrawFormattedText(video.window, right, video.screenXpixels * 0.75 - cond2rect(3)/2, video.screenYpixels * 0.30, video.black);
    
    spaceline = sprintf('[espace]');
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);
    
    Screen('Flip', video.window);
    WaitSecs(2);
    RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    
    %% INSTRUCTIONS 1 : piocher le plus de bleu :
    
    top = sprintf('D\0351mo "piocher un maximum de formes de la bonne couleur".\n\nVoici les sacs pour cette s\0351quence,\npiochez des formes bleues !');
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.20, video.black);
    
    targetsymbol = tutorialsqc(2).targetsymbol;
    othersymbol  = tutorialsqc(2).othersymbol;
    sidea        = 2;
    sideb        = 1;
    
    % Draw sequence bags:
    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sidea,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(sidea,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(sidea,:), 0, [], [], video.lbgd);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sideb,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(sideb,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(sideb,:), 0, [], [], video.lbgd);
    
    spaceline = sprintf('Appuyez sur [espace] pour commencer la s\0351quence.');
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);

    % Flip, wait 1'' for spacebar press
    Screen('DrawingFinished', video.window);
    Screen('Flip', video.window);
    WaitSecs(1);
    k = RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    
    % Flip fixation point for 1sec before starting
    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
    Screen('Flip', video.window);
    WaitSecs(1);
    
    for itrial = 1:ntrials
        dotrial(2, itrial)
    end
    
    %% INSTRUCTIONS 2 : deviner les couleurs :
    
    %%% sequence instructions:
    top = [sprintf('Bravo vous avez fini cette s\0351quence.\nMaintenant la d\0351mo pour "deviner la couleur des sacs".')...
           sprintf('\n\nVoici les sacs pour cette s\0351quence,\ndevinez la couleur de chacun !')];
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.20, video.black);
    
    targetsymbol = tutorialsqc(3).targetsymbol;
    othersymbol  = tutorialsqc(3).othersymbol;
    sidea        = 1;
    sideb        = 2;
    
    % Draw sequence bags:
    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sidea,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(sidea,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(sidea,:), 0, [], [], video.lbgd);
    Screen('DrawTexture', video.window, bagtx(2), [], bagpos(sideb,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(sideb,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(sideb,:), 0, [], [], video.lbgd);
    
    spaceline = sprintf('Appuyez sur [espace] pour commencer la s\0351quence.');
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);

    % Flip, wait 1'' for spacebar press
    Screen('DrawingFinished', video.window);
    Screen('Flip', video.window);
    WaitSecs(1);
    k = RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    
    % Flip fixation point for 1sec before starting
    Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
    Screen('Flip', video.window);
    WaitSecs(1);
    
    %%% loop on trials
    for itrial = 1:ntrials
        dotrial(3, itrial)
    end
    
    %%% final probe: which colour was the target bag? (the bag of circles is the target: it's drawing orange)
    blueside = 1;
    orangeside = 2;
    top = sprintf('Apr\0350s quelques tirages la s\0351quence s''arr\0352te : vous devez donner la couleur d''un des deux sacs :');
    line = sprintf('Pensez-vous que ce sac contenait surtout des formes oranges ou des formes bleues ?');
    DrawFormattedText(video.window,  top, 'center', video.screenYpixels*0.15, video.black);
    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, video.black);
    
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(3,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(3,:), 0, [], [], video.lbgd);
    
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(1,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(1,:), 0, [], [], orange);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(2,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(2,:), 0, [], [], blue);
    Screen('DrawingFinished', video.window);
    Screen('Flip', video.window);
    
    k = RestrictKeysForKbCheck(respKeys);
    while true
        [keyIsDown,secs,keylist] = KbCheck();
        if keyIsDown && (keylist(leftKey) == 1 ||  keylist(rightKey) == 1) && sum(keylist) == 1
            respprobe = find(respKeys == (find(keylist==1)));
            break
        end
    end
    
    % selection and feedback    
    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, video.black);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(3,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(3,:), 0, [], [], video.lbgd);
    
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(1,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(1,:), 0, [], [], orange);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(2,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(2,:), 0, [], [], blue);
    Screen('FrameRect', video.window, video.black, selection_rect(respprobe,:), 6);
    Screen('DrawingFinished', video.window);
    Screen('Flip', video.window); %show ASAP after selection
    WaitSecs(0.8);
    
    % color feedback on final probe:
    tcol = orange;
    DrawFormattedText(video.window,  line, 'center', video.screenYpixels*0.20, video.black);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(3,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(3,:), 0, [], [], tcol);
    Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], respshapepos(3,:), 0, [], [], [0 0 0]);
    Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], respshapepos(3,:), 0, [], [], video.lbgd);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(1,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(1,:), 0, [], [], orange);
    Screen('DrawTexture', video.window, bagtx(2), [], respbagpos(2,:), 0, [], [], [0,0,0]);
    Screen('DrawTexture', video.window, bagtx(1), [], respbagpos(2,:), 0, [], [], blue);
    Screen('FrameRect', video.window, video.black, selection_rect(respprobe,:), 6);

    Screen('DrawingFinished', video.window);
    Screen('Flip', video.window);
    WaitSecs(1)
    
    % end of sequence:
    top = sprintf('Bravo, c''est la fin de cette s\0351quence, vous connaissez les deux consignes !');
    DrawFormattedText(video.window, top, 'center', video.screenYpixels * 0.25, video.black);
    spaceline = sprintf('[espace]');
    DrawFormattedText(video.window, spaceline, 'center', video.screenYpixels * 0.90, video.black);
    Screen('Flip', video.window);
    WaitSecs(1);
    k = RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    
    %% PRECISIONS:
    text = [sprintf('Naturellement, pour que la t\0342che soit un peu plus difficile il y a quelques complications :')...
           sprintf('\n\n1. la couleur des formes que vous piochez est plus ou moins franche,\n    elle vous donne plus ou moins d''information sur la couleur du sac.')...
           sprintf('\n\n2. les sacs ne sont pas bien tri\0351s : il y aura des formes bleues dans les sacs oranges,\n    et inversement !')...
           sprintf('\n\n3. vous ne pouvez pas deviner la couleur d''un sac \0340 partir de celle du deuxi\0350me.\n    Ils peuvent \0352tre :\n    - un bleu / un orange,\n    - tous les deux bleus,\n    - tous les deux oranges.')...
           sprintf('\n\n4. les sacs changent de couleur \0340 chaque nouvelle s\0351quence.')...
           ];
    DrawFormattedText(video.window, text, video.screenYpixels * 0.1, video.screenYpixels * 0.20, video.black);
    
    conclusion = [ sprintf('Voil\0340 : si vous avez des questions, c''est le moment,\nsinon, vous pouvez passer \0340 l''entra\0356nement !')...
                   sprintf('\n\n[espace]')];
    DrawFormattedText(video.window, conclusion, 'center', video.screenYpixels * 0.80, video.black);
           
    Screen('Flip', video.window);
    WaitSecs(2);
    k = RestrictKeysForKbCheck(spaceKey);
    WaitKeyPress(spaceKey);
    
    Screen('FillRect', video.window, video.lbgd);
    Screen('Flip', video.window);

    header.end = datestr(now,'yyyymmdd-HHMM');
    
    k = RestrictKeysForKbCheck([]);

catch ME
    
    header.err = ME;
    rethrow(ME)
    
end % try/catch

    function dotrial(isq, itrial)
        
        targetside  = tutorialsqc(isq).symbolsides(itrial);
        otherside   = 3 - tutorialsqc(isq).symbolsides(itrial);
        Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
        
        Screen('DrawTexture', video.window, bagtx(2), [], bagpos(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(targetside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(targetside,:), 0, [], [], video.lbgd);
        
        Screen('DrawTexture', video.window, bagtx(2), [], bagpos(otherside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(otherside,:), 0, [], [], [0,0,0]);
        Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(otherside,:), 0, [], [], video.lbgd);

        Screen('DrawingFinished', video.window);
        
        % Before next flip, check for abort key:
         k = RestrictKeysForKbCheck([]);
        if CheckKeyPress(escapeKey)
            Priority(0); FlushEvents; ListenChar(0); ShowCursor;
            WaitSecs(1);
        end
        vbl = Screen('Flip', video.window);
        
        % Get sampling choice:
        k = RestrictKeysForKbCheck(respKeys);
        while true
            [keyIsDown,secs,keylist] = KbCheck();
            if keyIsDown && sum(keylist) == 1 && (keylist(leftKey) == 1 || keylist(rightKey) == 1)
                choice = find(respKeys == (find(keylist==1))); % side
                break
            end
        end
        
        % draw color outcome:
        if choice == targetside
            samplecolor = COLREP_fx_get_rgb(tutorialsqc(isq).colorslin(1,itrial));
            Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
            Screen('DrawTexture', video.window, shapetx(2, targetsymbol), [], shapepos(targetside,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(1, targetsymbol), [], shapepos(targetside,:), 0, [], [], samplecolor);
        elseif choice == otherside
            samplecolor = COLREP_fx_get_rgb(tutorialsqc(isq).colorslin(2,itrial));
            Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
            Screen('DrawTexture', video.window, shapetx(2, othersymbol), [], shapepos(otherside,:), 0, [], [], [0,0,0]);
            Screen('DrawTexture', video.window, shapetx(1, othersymbol), [], shapepos(otherside,:), 0, [], [], samplecolor);
        end
        
        % flip outcome for 0.5''
        Screen('Flip', video.window);
        WaitSecs(0.5);
        
        % draw fxpoint for 0.5''
        Screen('DrawDots', video.window, [video.dotx video.doty], 15 , video.black, [0 0], 2);
        Screen('Flip', video.window);
        WaitSecs(0.5);
        
    end

end % function definition



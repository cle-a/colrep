function [rgbv] = COLREP_fx_get_rgb(v)
    % get R/G/B guns
    % -1 is blue +1 is orange, 0 is grey
    v = (v+1)/2; % rescale between 0 and 1
    v = min(max(v,0),1);
    lumifg = 0.5;    
%     rgbcolors = [0.8,0,0.2; 0,0.8,0.2]; % green to magenta now
    rgbcolors = [1,lumifg,lumifg*2-1; lumifg*2-1,lumifg,1]; % rgb colors orange and blue
    rgbv = v*rgbcolors(1,:) + (1-v)*rgbcolors(2,:); % v is orange + rest is blue
    % so positives (rescaled to 1s) will be orange, negatives (0s) will be blue
end
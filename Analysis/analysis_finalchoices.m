%% check learning in condition 2 = final choice accuracy
% draws distribution of % correct at final choice per participant
% in condition 2, same or different sequences
% and divided per sequence length

clearvars;

lcol = lines;
dcol = lcol(4,:);
ocol = lcol(5,:);

load('choicestbl.mat');
choicestbl = choicestbl(choicestbl.excludepart == 0,:);
subj       = unique(choicestbl.subj);
nsubj      = numel(subj);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% subset the open sequences only & average proportion of correct final responses:
fprintf('collecting final choice correctness in each counterfactual subcondition\n');
cflevels       = unique(choicestbl.counterfactual); % = same:1 or different;2
lengths        = unique(choicestbl.nsamples);
finalchoicemat = nan(numel(subj), numel(cflevels), numel(lengths));
for ilen = 1:numel(lengths)
    for isubj = 1:numel(subj)
        for icf = 1:numel(cflevels)
            subset = choicestbl(choicestbl.condition == 2 & choicestbl.subj == subj(isubj) & choicestbl.nsamples == lengths(ilen) & choicestbl.counterfactual == cflevels(icf),:);
            finalchoicemat(isubj, icf, ilen) = nanmean(subset.finalchoicecorr, 1); % averaging
        end%for each symmetry
    end% for each subj
end% for each length

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% drawing distribution of participants scores
fprintf('plotting violins of participants scores\n');
figure('Color', [1 1 1],'units','centimeters','position',[10 10 10 10])

vp = violinplot(nanmean(finalchoicemat, 3), [1 2], 'Width', 0.2); % averaging on the seqlength dimension
for iviolon = 1:numel(vp)
    vp(iviolon).ViolinColor = 0.5 * (ocol + 1);
    vp(iviolon).EdgeColor   = 'none';
    vp(iviolon).ViolinAlpha = 0.3;
    vp(iviolon).ScatterPlot.SizeData = 70;
    vp(iviolon).ScatterPlot.MarkerFaceColor = ocol;
    vp(iviolon).ScatterPlot.MarkerFaceAlpha = 0.8;
end

% add chance
thr = 0.5;
xl = [0.5 2.5];
plot(linspace(xl(1),xl(2)), repelem(thr, 100), '--k') 

set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1]);
set(gca, 'XLim', xl, 'XTickLabel', {'same', 'different'});
set(gca,'FontName','Helvetica','FontSize',7.2);

pbar = 1;
set(gca, 'Layer', 'bottom', 'Box', 'off', 'PlotBoxAspectRatio', [pbar,1,1]);
set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));

axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

xlabel('condition 2', 'FontSize', 8);
ylabel('correct responses (%)', 'FontSize', 8);
title('percentage of correct responses at final probe', 'Fontweight', 'normal', 'FontSize', 8);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% violins per sequence lengths:
figure('Color', [1 1 1],'units','centimeters','position',[10 10 25 10])

for ilen = 1:numel(lengths)
    
    subplot(1,4,ilen)
    hold on
    vp = violinplot(finalchoicemat(:,:,ilen), [1 2], 'Width', 0.2);
    
    % customise violins
    for iviolon = 1:numel(vp)
        vp(iviolon).ViolinColor = 0.5 * (ocol + 1);
        vp(iviolon).EdgeColor   = 'none';
        vp(iviolon).ViolinAlpha = 0.3;
        vp(iviolon).ScatterPlot.SizeData = 70;
        vp(iviolon).ScatterPlot.MarkerFaceColor = ocol;
        vp(iviolon).ScatterPlot.MarkerFaceAlpha = 0.8;
    end
    
    % aesthetics
    thr = 0.5;
    xl = [0.5 2.5];
    plot(linspace(xl(1),xl(2)), repelem(thr, 100), '--k')
    
    set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1]);
    set(gca, 'XLim', xl, 'XTickLabel', {'same', 'different'});
    set(gca,'FontName','Helvetica','FontSize',7.2);
    
    pbar = 1;
    set(gca, 'Layer', 'bottom', 'Box', 'off', 'PlotBoxAspectRatio', [pbar,1,1]);
    set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));
    
    axes = findobj(gcf, 'type', 'axes');
    for a = 1:length(axes),
        if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
        if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
    end
    
    xlabel('condition 2', 'FontSize', 8);
    ylabel('correct responses (%)', 'FontSize', 8);
    title(sprintf('sequence length: %d', lengths(ilen)), 'Fontweight', 'normal', 'FontSize', 8)
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% test if sample is above chance:
x = nanmean(finalchoicemat,3); % average over lenghts
[h, p, ci] = ttest(x(:,1), 0.5) % 1 rejects H0 that sample is at mean 50 in 'not counterfactual = same'
[h, p, ci] = ttest(x(:,2), 0.5) % 1 rejects H0 that sample is at mean 50 in 'counterfactual     = different'

% %%% find participants at thr:
% thr = 0.50;
% x = nanmean(finalchoicemat,3); % average over lenghts
% subj(x(:,1) <= thr) % average of same 
% subj(x(:,2) <= thr) % average of different








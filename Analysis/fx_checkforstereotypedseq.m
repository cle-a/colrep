function [choicestbl] = fx_checkforstereotypedseq(choicestbl)
%%% checking if participants had stereotyped behaviour
%%% like choosing always the same symbol/side or systematically alternating
%%% exclude participants who's percentage of such sequences is above a given threshold (thr) in any condition

fprintf('...adding exclusion criterion\n');

lcol = lines;
col(1,:) = lcol(4,:);
col(2,:) = lcol(5,:);

thr = 50;

%% count bad sequences per subj
% bad = always the same symbol OR alternates each time 1/2/1/2/1/2 etc
% bad = always the same side, OR alternates at each choice

subjn = unique(choicestbl.subj);
nsubj = numel(subjn);

for isubj = 1:nsubj
    subj        = subjn(isubj);
    subsettbl   = choicestbl(choicestbl.subj == subj,:);
    for irow = 1:height(subsettbl)
        
        %%% regarding symbols:
        choices       = subsettbl.choices{irow}(~isnan(subsettbl.choices{irow})); % remove trailing nans
        switchsymbol  = diff(choices) ~= 0;     % 0 if stayed, 1 if switched
        alw(irow)     = sum(switchsymbol) == 0; % all possible switch points were non-switches = stayed all the time
        alt(irow)     = sum(switchsymbol) == (subsettbl.nsamples(irow)-1); % all possible switchpts were switches = alternated all the time
        
        %%% regarding side of the button:
        choicessides  = subsettbl.choicessides{irow}(~isnan(subsettbl.choicessides{irow}));
        switchside    = diff(choicessides) ~= 0;
        alwside(irow) = sum(switchside) == 0;
        altside(irow) = sum(switchside) == (subsettbl.nsamples(irow)-1);
        
    end % for each row
    
    for icond = 1:2
        cond = (subsettbl.condition == icond);
        subjpct(isubj, 1, icond) = sum(alw(cond)) / sum(subsettbl.condition == icond) * 100; %     1 = always same option
        subjpct(isubj, 2, icond) = sum(alt(cond)) / sum(subsettbl.condition == icond) * 100; %     2 = alternating between options
        subjpct(isubj, 3, icond) = sum(alwside(cond)) / sum(subsettbl.condition == icond) * 100; % 3 = always same side
        subjpct(isubj, 4, icond) = sum(altside(cond)) / sum(subsettbl.condition == icond) * 100; % 4 = alternates between sides
        subjpct(isubj, 5, icond) = sum(alw(cond) | alt(cond) | alwside(cond) | altside(cond)) / sum(subsettbl.condition == icond) * 100; % any of the BAD SEQUENCES
        subjpct(isubj, 6, icond) = subj;
    end
    
    clearvars alw alt alwside altside
    
end % for each subj

%% find participant's nbs
excluded_part = unique([subjpct(subjpct(:,5,1) >= thr, 6, 1); subjpct(subjpct(:,5,2) >= thr, 6, 2)]); % find wh participants are excluded in any condition
if ~isempty(excluded_part)
    var = zeros(size(choicestbl(:,1)));
    for is = 1:size(excluded_part,1)
        s = excluded_part(is);
        var(choicestbl.subj == s) = 1;
        choicestbl.excludepart = var;
    end
    stringex = sprintf('excluded %s participants', num2str(numel(excluded_part)));
else
    choicestbl.excludepart = zeros(size(choicestbl(:,1)));
    stringex = sprintf('excluded none');
end

%% plot percentage of bad sequences in each condition

addpath violin

figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);
vp = violinplot(squeeze(subjpct(:,5,:)), {'cond 1','cond 2'});
for icat = 1:numel(vp)
    vp(icat).ViolinColor = 0.5 * (lcol(7,:) + 1);
    vp(icat).EdgeColor   = 'none';
    vp(icat).ViolinAlpha = 0.3;
    vp(icat).ScatterPlot.SizeData = 70;
    vp(icat).ScatterPlot.MarkerFaceColor = lcol(7,:);
    vp(icat).ScatterPlot.MarkerFaceAlpha = 0.8;
end

plot([0.5:0.5:2.5], repelem(thr,size([0.5:0.5:2.5],2)), '--k')

set(gca, 'YLim', [0 100], 'YTick', [0:20:100]);

pbar = 1;
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
ylabel('percentage of bad sequences','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end
title('percentage bad sequences per participant', 'Fontweight', 'normal');

text(1.25, 75, stringex, 'FontSize', 8)

end





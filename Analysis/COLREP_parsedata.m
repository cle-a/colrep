clearvars;

subjn = [1:5];
nsubj = numel(subjn);

choicestbl = array2table(zeros(128*nsubj,1));
choicestbl.Properties.VariableNames = {'rawrow'};

row = 0;
for isubj = 1:nsubj
    
    %%% load subj data:
    clearvars -except subjn isubj subj choicestbl row
    subj = subjn(isubj);
%     load(sprintf('../Data/S%02d/S%02d_expe.mat', subj, subj), 'sampling', 'final', 'sqc');
    load(sprintf('../Data/Simulations/S103_expe_%d.mat', subj));
    final    = expe.final;
    sampling = expe.sampling;
    sqc      = expe.sqc;
    
    data = struct;
    nsq = numel(unique([final.isqc]));
    
    %%% go through each sequence to gather info:
    for sq = 1:nsq
        row = row+1;
        choicestbl.rawrow(row) = row;
            
        %%% DESIGN INFORMATIONS:
        choicestbl.subj(row)         = subj;
        choicestbl.seq(row)          = sq;
        choicestbl.blk(row)          = sqc(sq).blk;
        choicestbl.blkseq(row)       = sqc(sq).blkseq;
        choicestbl.condition(row)    = sqc(sq).condition;
        choicestbl.symmetry(row) = sqc(sq).symmetry;
        switch choicestbl.condition(row)
            case 1
                switch choicestbl.symmetry(row)
                    case 1; choicestbl.syncond(row)        = 1; % 1 = target good good
                            choicestbl.counterfactual(row) = 0;
                    case 2; choicestbl.syncond(row)        = 2; % 2 = target bad bad
                            choicestbl.counterfactual(row) = 0;
                    case 3; choicestbl.syncond(row)        = 3; % 3 = target good bad
                            choicestbl.counterfactual(row) = 1;
                end
            case 2
                switch choicestbl.symmetry(row)
                    case 1; choicestbl.syncond(row)        = 4; % 5 = open good good
                            choicestbl.counterfactual(row) = 0;
                    case 2; choicestbl.syncond(row)        = 5; % 6 = open bad bad
                            choicestbl.counterfactual(row) = 0;
                    case 3; choicestbl.syncond(row)        = 6; % 7 = open good bad
                            choicestbl.counterfactual(row) = 1;
                end
        end
        
        choicestbl.targetcolor(row)  = sqc(sq).targetcolor;
        choicestbl.nsamples(row)     = sqc(sq).nsamples;
        choicestbl.mirroring(row)    = sqc(sq).mirroring;
        choicestbl.coloroptions{row} = sqc(sq).colorslin;
        
        %%% PARTICIPANT'S SAMPLING INFORMATION
        % NB: choices are stored as 1 = target 2 = other
        %     sides are 1 = left 2 = right
        % NB: for "bad" sequences, participants are right to choose the "other" option
        % so flipping for sequences bad-good...
        choices                               = final(sq).choices; % sampling responses
        choices(final(sq).nsamples+1:20)      = NaN;        
        choicestbl.choices{row}               = choices;
        
        choicesrt                             = final(sq).choicesrt; % sampling response times
        choicesrt(final(sq).nsamples+1:20)    = NaN;        
        choicestbl.choicesrt{row}             = choicesrt;
        
        choicessides                          = final(sq).choicessides; % sides of sampling responses
        choicessides(final(sq).nsamples+1:20) = NaN;        
        choicestbl.choicessides{row}          = choicessides;
        
        colorseen = nan(2,choicestbl.nsamples(row));
        for ismp = 1:choicestbl.nsamples(row) % colours seen at each sampling response
            colorseen(choices(ismp), ismp)    = choicestbl.coloroptions{row}(choices(ismp), ismp);
        end
        colorseen(:,final(sq).nsamples+1:20)  = NaN;
        choicestbl.colorseen{row}             = colorseen;
                
        %%% FINAL PROBE INFORMATION:
        % /•/•/ NB: reshapes the response format into corr/incorr \•\•\
        % get final response "side" wrt task generation script - in open only: 1 = target color / 2 = other color
        % goodanswerside is the side where final(sq).targetcolor was presented
        % so it's the color of the targetsymbol *if* it was generated in the "good" color, for "bad", needs flipping:
        % NB: in bad-bad open sequences the underlying color was actually the opposite of the final(sq).targetcolor field
        % so in bad-bad the "correct" answer (= the true underlying color) is on badanswerside!
        choicestbl.finalchoiceside(row) = final(sq).finalresponseside;      % 1=left / 2=right
        choicestbl.finalchoice(row) = final(sq).finalresponse;              % 1=theoreticaltargetcolor side / 2=otherside
        if  choicestbl.condition(row) == 2 && choicestbl.symmetry(row) == 2 % 1=actualcolorgenerated / 2=othercolor
            choicestbl.finalchoicecorr(row) = final(sq).finalresponseside == 3-final(sq).finaltargetcolorside;
        else
            choicestbl.finalchoicecorr(row) = final(sq).finalresponseside == final(sq).finaltargetcolorside;
        end
        
        %%% NORMATIVE SAMPLING:
        % add choice by choice normative sampling info = what was the optimal choice at each sampling decision, based on the previous choices of the participant
        % i.e. as if they learnt and chose perfectly well given the information they had already received.
        % this is done sequence by sequence here...
        normchoices  = [1 nan(1, choicestbl.nsamples(row) -1)]; % first choice is given as optimal for free
        for ismp = 2:choicestbl.nsamples(row)
            values = nanmean(choicestbl.colorseen{row}(:, 1:ismp-1),2);
            values(isnan(values)) = 0; % if one was never choosen => 0
            switch choicestbl.condition(row)
                case 1 % immediate: choose the most blue or orange:
                    switch choicestbl.targetcolor(row)
                        case 1; [~, normchoices(ismp)] = min(values);
                        case 2; [~, normchoices(ismp)] = max(values);
                    end
                case 2 % delayed: choose that with lowest info level:
                    [~, normchoices(ismp)] = min(abs(values));
            end%switch on conditions
        end%for samples
        normchoices(:, choicestbl.nsamples(row)+1:20)  = NaN;
        choicestbl.normchoices{row} = normchoices;
        
    end%FOR each sequence
    fprintf('done subj #%d\n', subj)
end%FOR each subj

choicestbl = fx_checkforstereotypedseq(choicestbl);

save('choicestbl.mat', 'choicestbl')





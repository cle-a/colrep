%% making choice-choice similarity matrices:
% from choicestbl.mat:
% for each participant, draw the average 20x20 similarity matrix per "synthetic" condition (= condition x symmetry)
% for each participant, for each condition, compute similarity of each choice in each sequence,
% then average within condition
% and display as imgsc
% NB: function works by fieldname, so works for participants' responses as well as normative responses

clearvars;
load('../choicestbl.mat');
choicestbl = choicestbl(choicestbl.excludepart == 0,:);

fieldtoplot = 'choices';
figure('Name','AVERAGE CHOICE-CHOICE SIMILARITY OF PARTICIPANTS', 'Position', [0, 0, 1200, 400], 'Color', 'white');
hdl = fx_makerespmatrices(choicestbl, fieldtoplot)

fieldtoplot = 'choicessides';
figure('Name','AVERAGE CHOICE SIDE SIMILARITY OF PARTICIPANTS', 'Position', [0, 0, 1200, 400], 'Color', 'white');
hdl = fx_makerespmatrices(choicestbl, fieldtoplot)

fieldtoplot = 'normchoices';
figure('Name','AVERAGE OPTIMAL CHOICE SIMILARITY OF PARTICIPANTS', 'Position', [0, 0, 1200, 400], 'Color', 'white');
hdl = fx_makerespmatrices(choicestbl, fieldtoplot)

% save:
% saveas(gcf, '../matrices_participants.png')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [fighandle] = fx_makerespmatrices(choicestbl, fieldtoplot)

nsubj            = numel(unique(choicestbl.subj));
nsyncond         = numel(unique(choicestbl.syncond)); % keep 6 syncond so they each have the same nb of sequences, will average afterwards
nsmp             = max(unique(choicestbl.nsamples));  % max length of sequences = 20
choicematrix     = num2cell(nan(nsyncond, nsubj)); % because not all syncond have the same nb of seq: [nx20] x 6 x 30ish
similaritymatrix = nan(nsmp, nsmp, nsyncond, nsubj);  % 20 x 20 x 6 x 30ish

for isubj = 1:nsubj
    clearvars -except choicestbl fieldtoplot nsubj nsyncond nsmp choicematrix similaritymatrix isubj 
        
    % for each condition: (1)subset, (2)compute similarity of each choice within condition, (3) average per condition:
    for icond = 1:nsyncond        
        % (1) subset
        condsqc           = choicestbl(choicestbl.subj == isubj & choicestbl.syncond == icond, :);
        nsqcpersyncond    = height(condsqc); % nb of sequence per syncondition varies between syncond
        fidx              = find(strcmp(condsqc.Properties.VariableNames, fieldtoplot) == 1);
        condchoices       = cell2mat(table2cell(condsqc(:, fidx))); % get all the responses for all the sequences = 16 x 20
        condsimilaritymat = nan(nsmp, nsmp, nsqcpersyncond); % 20 x 20 similarity matrix for each sequence of the condition => 20 x 20 x nsqcpercond
        
        % (2) for each sequence, compute the 20 x 20 similarity matrix to all other choices:
        for iseq = 1:nsqcpersyncond
            
            % for each choice, see how similar it is to other choices *in its sequence*:
            for ismp = 1:condsqc.nsamples(iseq)
                sim = condchoices(iseq,ismp) - condchoices(iseq,:);
                
                corr_similaritymat = nan(size(sim)); % manually check for presence of nans:
                for isim = 1:numel(sim)
                    if isnan(sim(isim)); corr_similaritymat(isim) = NaN; % if nan stays nan
                    elseif sim(isim) == 0; corr_similaritymat(isim) = 1; % if 0 = they were the equal
                    elseif sim(isim) == 1 || sim(isim) == -1; corr_similaritymat(isim) = 0; % if not zero (1 or -1), were not equal
                    end %if nan, 0, 1
                end%for each isim value check for nans
                
                % for each smp, returns 20 values of its sim to all 20smp, so it's 20x20 for each sequence of this condition => 20 x 20 x nsqcpercond (16)
                condsimilaritymat(ismp, :, iseq) = corr_similaritymat; 
            
            end% for each sample, get a line of 20 similarity values
        end% for each sq in condition get 20x20 similarity matrix
        
        % (3) compute average similarity per synthetic condition:
        choicematrix{icond, isubj}        = (condchoices); % (a) matrix of the choices = [nsq x nsmp] x ncond x nsubj
        similaritymatrix(:,:,icond,isubj) = nanmean(condsimilaritymat,3); % (b) average similarity at each smp position *across sequences in condition* = 20x20 x ncond x nsubj

    end% for each synthetic condition
end% for each subj


%%% average response matrices and draw them across participants:
% adding 7th condition: mean of open bad/bad and open good/good as SAME:
similaritymatrix(:,:,7,:) = nanmean(similaritymatrix(:,:,4:5,:),3);

% average over participants:
averagesimilarity = nanmean(similaritymatrix, 4);

% draw:
lim = [0 1];
rgb = colormap('parula');
rgb = cat(1,[1,1,1],rgb);
colormap(rgb);

subplot(2,3,1);
x = averagesimilarity(:,:,3);
imagesc(x);
imagesc(tril(x,-1));
y = colorbar;
ylabel(y, 'mean response similarity', 'Fontsize', 10);
xlabel('sample position in sequence', 'Fontsize', 10);
set(gca,'CLim',lim,'PlotBoxAspectRatio',[1,1,1],'TickDir','out', 'Box', 'off');
set(gca,'FontSize', 10);
title({'immediate reward','GOOD/BAD'}, 'Fontsize', 14, 'FontWeight', 'normal');

subplot(2,3,2);
x = averagesimilarity(:,:,1);
imagesc(x);
imagesc(tril(x,-1));
y = colorbar;
ylabel(y, 'mean response similarity', 'Fontsize', 10);
xlabel('sample position in sequence', 'Fontsize', 10);
set(gca,'CLim',lim,'PlotBoxAspectRatio',[1,1,1],'TickDir','out', 'Box', 'off');
set(gca,'FontSize', 10);
title({'immediate reward','GOOD/GOOD'}, 'Fontsize', 14, 'FontWeight', 'normal');

subplot(2,3,3);
x = averagesimilarity(:,:,2);
imagesc(x);
imagesc(tril(x,-1));
y = colorbar;
ylabel(y, 'mean response similarity', 'Fontsize', 10);
xlabel('sample position in sequence', 'Fontsize', 10);
set(gca,'CLim',lim,'PlotBoxAspectRatio',[1,1,1],'TickDir','out', 'Box', 'off');
set(gca,'FontSize', 10);
title({'immediate reward','BAD/BAD'}, 'Fontsize', 14, 'FontWeight', 'normal');

subplot(2,3,4);
x = mean(averagesimilarity(:,:,6),3);
imagesc(x);
imagesc(tril(x,-1));
y = colorbar;
ylabel(y, 'mean response similarity', 'Fontsize', 10);
xlabel('sample position in sequence', 'Fontsize', 10);
set(gca,'CLim',lim,'PlotBoxAspectRatio',[1,1,1],'TickDir','out', 'Box', 'off');
set(gca,'FontSize', 10);
title({'immediate reward','DIFFERENT'}, 'Fontsize', 14, 'FontWeight', 'normal');

subplot(2,3,5);
x = mean(averagesimilarity(:,:,7),3);
imagesc(x);
imagesc(tril(x,-1));
y = colorbar;
ylabel(y, 'mean response similarity', 'Fontsize', 10);
xlabel('sample position in sequence', 'Fontsize', 10);
set(gca,'CLim',lim,'PlotBoxAspectRatio',[1,1,1],'TickDir','out', 'Box', 'off');
set(gca,'FontSize', 10);
title({'immediate reward','SAME'}, 'Fontsize', 14, 'FontWeight', 'normal');

fighandle = gcf;

end
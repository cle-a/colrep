%%% plot parameter estimates
%%% load a dat_modelbound_boundfix_*.mat

close all
clc

load('dat_modelbound_boundfix_beta.mat') % specify which model to choose 
load('../../../COLEXP/Analysis/Valentin/dat_modelbound_boundfit_beta.mat')

addpath ../violin

lcol = lines;
col(1,:) = lcol(4,:);
col(2,:) = lcol(5,:);

%% sensitivity to target color-ness = betav (only defined in condition 1)
figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);

vpv = violinplot(betav_fit(:,1), {'condition 1'});

vpv.ViolinColor = 0.5 * (col(1,:) + 1);
vpv.EdgeColor   = 'none';
vpv.ViolinAlpha = 0.3;
vpv.ScatterPlot.SizeData        = 70;
vpv.ScatterPlot.MarkerFaceColor = col(1,:);
vpv.ScatterPlot.MarkerFaceAlpha = 0.8;

plot([0.5:0.5:1.5], repelem(0,size([0.5:0.5:1.5],2)), '--k')

pbar = 1;
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
ylabel('estimated parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

[h, p, ci] = ttest(betav_fit(:,1), 0) % 1 rejects H0 that sample is no different to 50

%% sensitivity to information value = betac (in both conditions)
figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);

vpc = violinplot(betac_fit, {'cond 1', 'cond 2'});

for icat = 1:numel(vpc)
    vpc(icat).ViolinColor = 0.5 * (col(icat,:) + 1);
    vpc(icat).EdgeColor   = 'none';
    vpc(icat).ViolinAlpha = 0.3;
    vpc(icat).ScatterPlot.SizeData        = 70;
    vpc(icat).ScatterPlot.MarkerFaceColor = col(icat,:);
    vpc(icat).ScatterPlot.MarkerFaceAlpha = 0.8;
end

ylim([-1,+1]);
set(gca, 'YTick', [-1:0.5:1])

plot([0.5:0.5:2.5], repelem(0,size([0.5:0.5:2.5],2)), '--k')

pbar = 1;
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
ylabel('estimated parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

[h, p, ci] = ttest(betac_fit(:,1), 0) % 1 rejects H0 that sample is no different to 50
[h, p, ci] = ttest(betac_fit(:,2), 0) % 1 rejects H0 that sample is no different to 50


%% sensitivity to repetition bias = betar (in both conditions)
figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);

vpr = violinplot(betar_fit, {'cond1', 'cond2'});

for icat = 1:numel(vpr)
    vpr(icat).ViolinColor = 0.5 * (col(icat,:) + 1);
    vpr(icat).EdgeColor   = 'none';
    vpr(icat).ViolinAlpha = 0.3;
    vpr(icat).ScatterPlot.SizeData        = 70;
    vpr(icat).ScatterPlot.MarkerFaceColor = col(icat,:);
    vpr(icat).ScatterPlot.MarkerFaceAlpha = 0.8;
end

plot([0.5:0.5:2.5], repelem(0,size([0.5:0.5:2.5],2)), '--k')

pbar = 1;
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
ylabel('estimated parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

[h, p, ci] = ttest(betar_fit(:,1), 0) % 1 rejects H0 that sample is no different to 50
[h, p, ci] = ttest(betar_fit(:,2), 0) % 1 rejects H0 that sample is no different to 50

%% sensitivity to the bound = betab (for both conditions)
%  NB: it was fitted by setting the bound to the same value in both conditions
%  NB: you will need to change this to epsib_fit if fitting with an epsilon-style parameter
figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);

vpb = violinplot(betab_fit, {'cond1', 'cond2'});

for icat = 1:numel(vpr)
    vpb(icat).ViolinColor = 0.5 * (col(icat,:) + 1);
    vpb(icat).EdgeColor   = 'none';
    vpb(icat).ViolinAlpha = 0.3;
    vpb(icat).ScatterPlot.SizeData        = 70;
    vpb(icat).ScatterPlot.MarkerFaceColor = col(icat,:);
    vpb(icat).ScatterPlot.MarkerFaceAlpha = 0.8;
end

plot([0.5:0.5:2.5], repelem(0,size([0.5:0.5:2.5],2)), '--k')

pbar = 1;
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
ylabel('estimated parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

[h, p, ci] = ttest(betab_fit(:,1), 0) % 1 rejects H0 that sample is no different to 50
[h, p, ci] = ttest(betab_fit(:,2), 0) % 1 rejects H0 that sample is no different to 50


function [out] = modelfit_fit_modelbound(cfg)
%  FIT_MODELBOUND  Fit choice model with soft sampling bound
%  either sensitivity to the bound is governed by a softmax ('beta')
%  or by an epsilon-style term ('epsi')

% check configuration structure
if ~all(isfield(cfg,{'conditn','targetc','colvals','choices','bound'}))
    error('incomplete configuration structure!');
end
if ~isfield(cfg,'btype')
    cfg.btype = 'beta';
end
if ~isfield(cfg,'nsim')
    cfg.nsim = [];
end
if ~ismember(cfg.btype,{'beta','epsi'})
    error('undefined bound type!');
end

% define model parameters
npar = 0;
pnam = {};
pini = [];
pmin = [];
pmax = [];
% choice sensitivity to target value
npar = npar+1;
pnam{npar,1} = 'betav';
pini(npar,1) = 0;
pmin(npar,1) = -10;
pmax(npar,1) = +10;
% choice sensitivity to information value
npar = npar+1;
pnam{npar,1} = 'betac';
pini(npar,1) = 0;
pmin(npar,1) = -10;
pmax(npar,1) = +10;
% choice repetition bias
npar = npar+1;
pnam{npar,1} = 'betar';
pini(npar,1) = 0;
pmin(npar,1) = -10;
pmax(npar,1) = +10;
% bound strength
npar = npar+1;
switch cfg.btype
    case 'beta' % softmax bound
        pnam{npar,1} = 'betab';
        pini(npar,1) = 0;
        pmin(npar,1) = 0;
        pmax(npar,1) = 10;
    case 'epsi' % epsilon bound
        pnam{npar,1} = 'epsib';
        pini(npar,1) = 0;
        pmin(npar,1) = 0;
        pmax(npar,1) = 1;
end

% do not fit sensitivity to target value in open sampling condition
if cfg.conditn == 2
    cfg.betav = 0;
end
% do not fit bound strength if bound-free
if cfg.bound == 0
    cfg.betab = 0;
    cfg.epsib = 0;
end

% define fixed parameters
pfix = nan(npar,1);
for i = 1:npar
    if isfield(cfg,pnam{i})
        pfix(i) = cfg.(pnam{i});
    end
end

% define fitted parameters
ifit = nan(npar,1);
pfit_ini = [];
pfit_min = [];
pfit_max = [];
n = 1;
for i = 1:npar
    if isnan(pfix(i))
        ifit(i) = n;
        pfit_ini = cat(1,pfit_ini,pini(i));  
        pfit_min = cat(1,pfit_min,pmin(i));
        pfit_max = cat(1,pfit_max,pmax(i));
        n = n+1;
    end
end
nfit = numel(pfit_ini);
nobs = sum(cellfun(@length,cfg.choices));

% fit free parameters
if nfit > 0
    pval = fmincon(@fmin,pfit_ini,[],[],[],[],pfit_min,pfit_max,[], ...
        optimset('Display','notify','FunValCheck','on','Algorithm','interior-point','TolX',1e-20,'MaxFunEvals',1e6));
    [~,phat] = fmin(pval);
else
    phat = num2cell(pfix);
end

% get best-fitting parameters
out = cell2struct(phat,pnam);

% get quality-of-fit metrics
out.llh = get_llh(phat{:}); % model log-likelihood
out.aic = -2*out.llh+2*nfit+2*nfit*(nfit+1)/(nobs-nfit+1); % model AIC
out.bic = -2* out.llh + nfit * log(nobs); % model BIC

% compute model-free metrics
[~,out.ndif,out.vopt,out.copt,out.mixt] = get_llh(phat{:});

%simulate model choices
if ~isempty(cfg.nsim)
    [out.mch,out.ndif_mod,out.vopt_mod,out.copt_mod] = sim_mch(phat{:});
end

% simulate optimal choices
[out.opt,out.ndif_opt,out.vopt_opt,out.copt_opt] = sim_opt();

    function [f,pfit] = fmin(p)
        % objective function = negative model log-likelihood
        pfit = cell(npar,1);
        for i = 1:npar
            if isnan(pfix(i))
                pfit{i} = p(ifit(i));
            else
                pfit{i} = pfix(i);
            end
        end
        f = -get_llh(pfit{:});
    end

    function [llh,ndif,vopt,copt,mixt] = get_llh(betav,betac,betar,parmb)
        % get model log-likelihood for parameter set
        nseq = numel(cfg.choices);
        ndif = cell(nseq,1);
        vopt = cell(nseq,1);
        copt = cell(nseq,1);
        lseq = nan(nseq,1);
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            nc = nan(nsmp+1,2);
            mc = nan(nsmp+1,2);
            pc = nan(nsmp,2);
            lc = nan(nsmp,1);
            for ismp = 1:nsmp
                if ismp == 1
                    nc(1,:) = 0;
                    mc(1,:) = 0;
                    pc(1,:) = 0.5;
                else
                    ch = cfg.choices{iseq}(ismp-1);
                    dv = betav*(mc(ismp,1)-mc(ismp,2))*cfg.targetc(iseq);
                    dv = dv+betac*(abs(mc(ismp,1))*nc(ismp,1)-abs(mc(ismp,2))*nc(ismp,2));
                    dv = dv+betar*(3-2*ch);
                    
                    switch cfg.btype
                        case 'beta' % softmax bound
                            dv = dv+parmb*(3-2*ch)*(nc(ismp,ch) <= cfg.bound);
                            pc(ismp,1) = 1./(1+exp(-dv));
                            pc(ismp,2) = 1-pc(ismp,1);
                        case 'epsi' % epsilon bound
                            pc(ismp,1) = 1./(1+exp(-dv));
                            pc(ismp,2) = 1-pc(ismp,1);
                            if nc(ismp,ch) <= cfg.bound
                                pc(ismp,ch) = parmb+(1-parmb)*pc(ismp,ch);
                                pc(ismp,3-ch) = 1-pc(ismp,ch);
                            end
                    end
                end
                ch = cfg.choices{iseq}(ismp);
                nc(ismp+1,ch) = nc(ismp,ch)+1;
                mc(ismp+1,ch) = mc(ismp,ch)+ ...
                    1/nc(ismp+1,ch)*(cfg.colvals{iseq}(ch,ismp)-mc(ismp,ch));
                nc(ismp+1,3-ch) = nc(ismp,3-ch);
                mc(ismp+1,3-ch) = mc(ismp,3-ch);
                lc(ismp) = log(max(pc(ismp,ch),realmin));
            end
            % compute model-free metrics
            if nargout > 1
                ndif{iseq} = abs(nc(2:end,1)-nc(2:end,2))';
                vopt{iseq} = cfg.choices{iseq} == 1+(( ...
                    mc(1:nsmp,1)-mc(1:nsmp,2))'*cfg.targetc(iseq) < 0);
                vopt{iseq} = double(vopt{iseq});
                vopt{iseq}(1) = nan;
                copt{iseq} = cfg.choices{iseq} == 1+(( ...
                    abs(mc(1:nsmp,1)).*nc(1:nsmp,1)- ...
                    abs(mc(1:nsmp,2)).*nc(1:nsmp,2))' > 0);
                copt{iseq} = double(copt{iseq});
                copt{iseq}(1) = nan;
                mixt{iseq} = mc(1:nsmp,:)';
            end
            lseq(iseq) = sum(lc);
        end
        llh = sum(lseq);
    end

    function [mch,ndif,vopt,copt] = sim_mch(betav,betac,betar,parmb)
        % simulate model choices
        nseq = numel(cfg.choices);
        mch  = cell(nseq,1);
        ndif = cell(nseq,1);
        vopt = cell(nseq,1);
        copt = cell(nseq,1);
        mixt = cell(nseq,1);
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            mch{iseq} = nan(cfg.nsim,nsmp);
            nc = nan(nsmp+1,2,cfg.nsim);
            mc = nan(nsmp+1,2,cfg.nsim);
            pc = nan(cfg.nsim,2);
            for ismp = 1:nsmp
                % compute decision variable
                if ismp == 1
                    nc(1,:,:) = 0;
                    mc(1,:,:) = 0;
                    % same 1st choice as subject
                    pc(:,1) = 1-(cfg.choices{iseq}(1) == 2);
                    pc(:,2) = 1-pc(:,1);
                else
                    ch = reshape(mch{iseq}(:,ismp-1),[1,1,cfg.nsim]);
                    dv = betav*(mc(ismp,1,:)-mc(ismp,2,:))*cfg.targetc(iseq);
                    dv = dv+betac*(abs(mc(ismp,1,:)).*nc(ismp,1,:)-abs(mc(ismp,2,:)).*nc(ismp,2,:));
                    dv = dv+betar*(3-2*ch);
                    switch cfg.btype
                        case 'beta'
                            i1 = ch == 1; i2 = ch == 2;
                            dv(i1) = dv(i1)+parmb*(nc(ismp,1,i1) <= cfg.bound);
                            dv(i2) = dv(i2)-parmb*(nc(ismp,2,i2) <= cfg.bound);
                            pc(:,1) = 1./(1+exp(-dv));
                            pc(:,2) = 1-pc(:,1);
                        case 'epsi'
                            pc(:,1) = 1./(1+exp(-dv));
                            pc(:,2) = 1-pc(:,1);
                            i1 = ch == 1 & nc(ismp,1,:) <= cfg.bound;
                            i2 = ch == 2 & nc(ismp,2,:) <= cfg.bound;
                            pc(i1,1) = parmb+(1-parmb)*pc(i1,1);
                            pc(i1,2) = 1-pc(i1,1);
                            pc(i2,2) = parmb+(1-parmb)*pc(i2,2);
                            pc(i2,1) = 1-pc(i2,2);
                    end
                end
                % choose
                ch = 1+(pc(:,2) > rand(cfg.nsim,1));
                mch{iseq}(:,ismp) = ch;
                i1 = ch == 1; i2 = ch == 2;
                % update learning variables for simulations where ch = 1
                nc(ismp+1,1,i1) = nc(ismp,1,i1)+1;
                mc(ismp+1,1,i1) = mc(ismp,1,i1)+ ...
                    1./nc(ismp+1,1,i1).*(cfg.colvals{iseq}(1,ismp)-mc(ismp,1,i1));
                nc(ismp+1,2,i1) = nc(ismp,2,i1);
                mc(ismp+1,2,i1) = mc(ismp,2,i1);
                % update learning variables for simulations where ch = 2
                nc(ismp+1,2,i2) = nc(ismp,2,i2)+1;
                mc(ismp+1,2,i2) = mc(ismp,2,i2)+ ...
                    1./nc(ismp+1,2,i2).*(cfg.colvals{iseq}(2,ismp)-mc(ismp,2,i2));
                nc(ismp+1,1,i2) = nc(ismp,1,i2);
                mc(ismp+1,1,i2) = mc(ismp,1,i2);
            end
            % compute model-free metrics
            if nargout > 1
                ndif{iseq} = permute(abs(nc(2:end,1,:)-nc(2:end,2,:)),[3,1,2]);
                vopt{iseq} = mch{iseq} == 1+(permute( ...
                    mc(1:nsmp,1,:)-mc(1:nsmp,2,:),[3,1,2])*cfg.targetc(iseq) < 0);
                vopt{iseq}(:,1) = false;
                copt{iseq} = mch{iseq} == 1+(permute( ...
                    abs(mc(1:nsmp,1,:)).*nc(1:nsmp,1,:)- ...
                    abs(mc(1:nsmp,2,:)).*nc(1:nsmp,2,:),[3,1,2]) > 0);
                copt{iseq}(:,1) = false;
            end
        end
    end

    function [opt,ndif,vopt,copt] = sim_opt()
        % simulate optimal choices
        nseq = numel(cfg.choices);
        opt  = cell(nseq,1);
        ndif = cell(nseq,1);
        vopt = cell(nseq,1);
        copt = cell(nseq,1);
        
        for iseq = 1:nseq
            nsmp = numel(cfg.choices{iseq});
            opt{iseq} = nan(1,nsmp);
            nc = nan(nsmp+1,2);
            mc = nan(nsmp+1,2);
            for ismp = 1:nsmp
                if ismp == 1
                    nc(1,:) = 0;
                    mc(1,:) = 0;
                    % same 1st choice as subject
                    ch = cfg.choices{iseq}(1);
                else
                    % choose
                    if cfg.conditn == 1
                        dv = (mc(ismp,1)*nc(ismp,1)-mc(ismp,2)*nc(ismp,2))*cfg.targetc(iseq);
                        ch = 1+(dv < 0); % argmax on target value
                    else
                        dv = abs(mc(ismp,1))*nc(ismp,1)-abs(mc(ismp,2))*nc(ismp,2);
                        ch = 1+(dv > 0); % argmin on information value
                    end
                end
                opt{iseq}(ismp) = ch;
                nc(ismp+1,ch) = nc(ismp,ch)+1;
                mc(ismp+1,ch) = mc(ismp,ch)+ ...
                    1/nc(ismp+1,ch)*(cfg.colvals{iseq}(ch,ismp)-mc(ismp,ch));
                nc(ismp+1,3-ch) = nc(ismp,3-ch);
                mc(ismp+1,3-ch) = mc(ismp,3-ch);
            end
            % compute model-free metrics
            if nargout > 1
                ndif{iseq} = abs(nc(2:end,1)-nc(2:end,2))';
                vopt{iseq} = opt{iseq} == 1+(( ...
                    mc(1:nsmp,1)-mc(1:nsmp,2))'*cfg.targetc(iseq) < 0);
                vopt{iseq}(1) = false;
                copt{iseq} = opt{iseq} == 1+(( ...
                    abs(mc(1:nsmp,1)).*nc(1:nsmp,1)- ...
                    abs(mc(1:nsmp,2)).*nc(1:nsmp,2))' > 0);
                copt{iseq}(1) = false;
            end
        end
    end

end
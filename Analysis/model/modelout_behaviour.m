%%% plots participants behaviour / model behaviour:
%%% choice-choice similarity matrices
%%% choice direction wrt information and wrt to targetness of the expected colour along the sequence
%%% sampling imbalance between the two options along the sequence
%%%
%%% LOADS: dat_modelbound_boundfix_*.mat
%%% so test_modelbound should be run before.

%% loading mat:
load('dat_modelbound_boundfix_beta.mat')
load('../../../COLEXP/Analysis/Valentin/dat_modelbound_boundfit_beta.mat')
load('../../../COLEXP/Analysis/Valentin/dat_modelbound_boundfix_epsi.mat')

%% plot choice-choice similarity matrix
close all
clc

icond = 1; % condition (1:directed 2:open)
ifact = 1; % counterfactualness (1:good/good 2:bad/bad 3:good/bad)

% plot choice-choice similarity matrix for subjects
pbar = 1;
figure('Color','white');
rgb = colormap('parula');
rgb = cat(1,[1,1,1],rgb);
colormap(rgb);
x = squeeze(mean(mean(chmat_sub(:,icond,ifact,:,:),3),1));
imagesc(tril(x,-1));
hold on
xlim(xlim);
ylim(ylim);
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',4:4:20,'YTick',4:4:20');
set(gca,'CLim',[0.2,0.8]);
xlabel('choice position','FontSize',8);
ylabel('choice position','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]
        axes(a).YColor = [0 0 0];
    end
    if axes(a).XColor <= [1 1 1]
        axes(a).XColor = [0 0 0];
    end
end

% plot choice-choice similarity matrix for model simulations
pbar = 1;
figure('Color','white');
rgb = colormap('parula');
rgb = cat(1,[1,1,1],rgb);
colormap(rgb);
x = squeeze(mean(mean(chmat_mod(:,icond,ifact,:,:),3),1));
imagesc(tril(x,-1));
hold on
xlim(xlim);
ylim(ylim);
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',4:4:20,'YTick',4:4:20');
set(gca,'CLim',[0.2,0.8]);
xlabel('choice position','FontSize',8);
ylabel('choice position','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]
        axes(a).YColor = [0 0 0];
    end
    if axes(a).XColor <= [1 1 1]
        axes(a).XColor = [0 0 0];
    end
end

% plot choice-choice similarity matrix for optimal decisions
pbar = 1;
figure('Color','white');
rgb = colormap('parula');
rgb = cat(1,[1,1,1],rgb);
colormap(rgb);
x = squeeze(mean(mean(chmat_opt(:,icond,ifact,:,:),3),1));
x = tril(x,-1);
imagesc(x);
hold on
xlim(xlim);
ylim(ylim);
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',4:4:20,'YTick',4:4:20');
set(gca,'CLim',[0,1]);
xlabel('choice position','FontSize',8);
ylabel('choice position','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

%% plot choice direction wrt target and certainty
%  thick lines +/- shaded error bars show subjects
%  thin lines show model simulations
%  purple lines show condition 1
%  green lines show condition 2

close all
clc

lcol = lines;
rgb(1,:) = lcol(4,:);
rgb(2,:) = lcol(5,:);

ifact = 1:3; % counterfactualness (1:good/good 2:bad/bad 3:good/bad)

% plot choice direction wrt target
xavg = squeeze(mean(mean(vopt_sub(:,:,ifact,2:end),3),1));
xerr = squeeze(std(mean(vopt_sub(:,:,ifact,2:end),3),[],1))/sqrt(nsubj)*1.96;
mavg = squeeze(mean(mean(vopt_mod(:,:,ifact,2:end),3),1));
x = 2:20;
pbar = 1;
figure('Color','white','units','centimeters','innerposition',[0 0 5 5]);
hold on
xlim([0.5,20]);
ylim([0,1]);
for i = 1:2
    patch([x,fliplr(x)],[xavg(i,:)+xerr(i,:),fliplr(xavg(i,:)-xerr(i,:))],0.5*(rgb(i,:)+1),'EdgeColor','none');
end
plot(xlim,[1,1]*0.5,'-','Color',[0.5,0.5,0.5], 'LineStyle', '--');
for i = 1:2
    plot(x,mavg(i,:),'-','LineWidth',1,'Color',rgb(i,:));
    plot(x,xavg(i,:),'-','LineWidth',2,'Color',rgb(i,:));
end
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',[4:4:20],'YTick',0:0.2:1);
set(gca,'CLim',[0.2,0.8]);
xlabel('choice position','FontSize',8);
ylabel('choice direction','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes)
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

% plot choice direction wrt certainty
xavg = squeeze(mean(mean(copt_sub(:,:,ifact,2:end),3),1));
xerr = squeeze(std(mean(copt_sub(:,:,ifact,2:end),3),[],1))/sqrt(nsubj)*1.96;
mavg = squeeze(mean(mean(copt_mod(:,:,ifact,2:end),3),1));
x = 2:20;
pbar = 1;
figure('Color','white','units','centimeters','innerposition',[0 0 16 5]);
hold on
xlim([0.5,20]);
ylim([0,1]);
for i = 1:2
    patch([x,fliplr(x)],[xavg(i,:)+xerr(i,:),fliplr(xavg(i,:)-xerr(i,:))],0.5*(rgb(i,:)+1),'EdgeColor','none');
end
plot(xlim,[1,1]*0.5,'-','Color',[0.5,0.5,0.5], 'LineStyle', '--');
for i = 1:2
    plot(x,mavg(i,:),'-','LineWidth',1,'Color',rgb(i,:));
    plot(x,xavg(i,:),'-','LineWidth',2,'Color',rgb(i,:));
end
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',[4:4:20],'YTick',0:0.2:1);
set(gca,'CLim',[0.2,0.8]);
xlabel('choice position','FontSize',8);
ylabel('choice direction','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes)
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

%% plot sampling imbalance
%  thick lines +/- shaded error bars show subjects
%  thin lines show model simulations
%  purple lines show condition 1
%  green lines show condition 2
close all
clc

load('dat_modelbound_boundfix_beta.mat')
lcol = lines;
rgb(1,:) = lcol(4,:);
rgb(2,:) = lcol(5,:);

ifact = 1:3; % counterfactualness (1:good/good 2:bad/bad 3:good/bad)

xavg = squeeze(mean(mean(ndif_sub(:,:,ifact,:),3),1));
xerr = squeeze(std(mean(ndif_sub(:,:,ifact,:),3),[],1))/sqrt(nsubj)*1.96;
mavg = squeeze(mean(mean(ndif_mod(:,:,ifact,:),3),1));
oavg = squeeze(mean(mean(ndif_opt(:,:,ifact,:),3),1));
x = 1:20;
pbar = 1;
figure('Color','white','units','centimeters','innerposition',[0 0 16 5]);
hold on
xlim([0.5,20]);
ylim([0,10]);
for i = 1:2
    patch([x,fliplr(x)],[xavg(i,:)+xerr(i,:),fliplr(xavg(i,:)-xerr(i,:))],0.5*(rgb(i,:)+1),'EdgeColor','none');
end
for i = 1:2
    plot(x,oavg(i,:),':','LineWidth',1,'Color',rgb(i,:));
    plot(x,mavg(i,:),'-','LineWidth',1,'Color',rgb(i,:));
    plot(x,xavg(i,:),'-','LineWidth',2,'Color',rgb(i,:));
end
hold off
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',[4:4:20],'YTick',0:2:10);
set(gca,'CLim',[0.2,0.8]);
xlabel('choice position','FontSize',8);
ylabel('sampling imbalance','FontSize',8);
axes = findobj(gcf, 'type', 'axes');

for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end


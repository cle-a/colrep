%%% Perform BMS on bound levels with SPM_BMS
%%%     using BIC to approximate model evidence
%%% Uses dat_modelbound_boundfit_*.mat
%%%     which is the output of test_modelbound if fitting bounds: bound = []
%%% Requires SPM toolbox installed and added to path
%%% purple lines show condition 1
%%% green lines show condition 2

clearvars
addpath('~/Documents/MATLAB/spm12/');

load('dat_modelbound_boundfit_beta.mat', 'bic_bound')
load('../../../COLEXP/Analysis/Valentin/dat_modelbound_boundfit_beta.mat', 'bic_bound')

lcol = lines;
dircol = lcol(4,:);
opncol = lcol(5,:);

%%% use BIC as approximate model evidence: modev = marginal likelihood = estimated from BIC
mev = -0.5 * bic_bound; % BIC = -2 * log(modevidence)
if any(isnan(mev(:)))
    error('the bound should have been fitted for this to work!');
end

%%%  perform random-effects BMS from SPM toolbox: returns exceedance probability 
pavg = nan(2,20);
pstd = nan(2,20);
pexc = nan(2,20);
for icond = 1:2
    for i = 1:20
        [alpha, ~, xp] = spm_BMS(squeeze(mev(:,icond,[1,i+1])));
        [avg_a, var_a] = drchstat(alpha);
        pavg(icond, i) = avg_a(2);
        pstd(icond, i) = sqrt(var_a(2,2));
        pexc(icond, i) = xp(2);
    end
end


%% plots

%%% (1) model frequency

figure('Color','white','units','centimeters','innerposition',[0 0 10 10]);
hold on

rgb(1,:) = dircol;
rgb(2,:) = opncol;
x = 1:20;

% for each condition:
for icond = 1:2
    % draw patch +/- 1SD:
    patch([x,fliplr(x)],[pavg(icond,:)+pstd(icond,:),fliplr(pavg(icond,:)-pstd(icond,:))],0.5*(rgb(icond,:)+1),'EdgeColor','none');
    % overlay average freq:
    plot(x,pavg(icond,:),'-','LineWidth',2,'Color',rgb(icond,:));
    % add exceedance probabilities:
    plot(x,pexc(icond,:),'--','LineWidth',1,'Color',rgb(icond,:));
end

xlim([0.5,20.5]);
ylim([0,1]);
pbar = 1;
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',[1,4:4:20],'YTick',0:0.2:1);
set(gca,'CLim',[0.2,0.8]);
xlabel('bound level','FontSize',8);
ylabel('model frequency','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes)
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

%saveas(gcf, 'modfrequency.png')



%%% (2) relative model evidence

figure('Color','white','units','centimeters','innerposition',[0 0 10 7.5]);
hold on

mev = bsxfun(@minus,mev(:,:,2:end),mev(:,:,1));
mavg = squeeze(mean(mev,1));
merr = squeeze(std(mev,[],1))/sqrt(size(mev,1));
x = 1:20;

% plot equal evidence
plot(xlim,[0,0],'Color', [0.5 0.5 0.5], 'LineStyle', '--', 'LineWidth', 0.5);
% plot curves
for i = 1:2
    patch([x,fliplr(x)],[mavg(i,:)+merr(i,:),fliplr(mavg(i,:)-merr(i,:))],0.5*(rgb(i,:)+1),'EdgeColor','none');
    plot(x,mavg(i,:),'-','LineWidth',2,'Color',rgb(i,:));
end
xlim([0.5,20]);
% ylim([-4,+8]);

pbar = 1.5;
set(gca,'Layer','top','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',[4:4:20],'YTick',-4:4:8);
set(gca,'CLim',[0.2,0.8]);
xlabel('bound level','FontSize',8);
ylabel('relative model evidence','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes)
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

%saveas(gcf, 'relativemodelevidence.png')



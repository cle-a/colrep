%%% This calls modelfit_fit_modelbound function to fit the model
%%% It should run twice: 
%%% (a) first with bound = [], fits bound values, outputs dat_modelbound_boundfiT
%%% (b) choose the best fitting bound value bv, based on bms_boundlevel script
%%% (c) run this code a second time : with fixed bound value = [bv bv]
%%%     This time it fits the other parameters, outputs dat_modelbound_boundfiX
%%%     By defaut the bound is set to bv in both conditions [bv bv],
%%%     and the epsi/beta parameters control the strength of the bound in each condition.
%%%
%%% We ran the code with beta bound type.

%% fit models:
clearvars
close all
clc

%%% init:
% define list of subjects
load('../choicestbl.mat')
subjlist = unique(choicestbl.subj(choicestbl.excludepart==0));
nsubj = numel(subjlist);

% define global model parameters
bound = [];     % condition-wise bound values ([]:fitted or [a,b]:fixed, for condition 1 and condition 2)
btype = 'beta'; % bound type (beta or epsi)
nsim  = 1e4;    % number of model simulations
nrec  = 1e2;    % number of model recovery tests (nrec <= nsim)

% model AIC and BIC for each bound level (if fitted)
aic_bound = nan(nsubj,2,20);
bic_bound = nan(nsubj,2,20); % we used BIC

% best-fitting model parameters
bound_fit = nan(nsubj,2); % bound level
betav_fit = nan(nsubj,2); % choice sensitivity to target value
betac_fit = nan(nsubj,2); % choice sensitivity to information value
betar_fit = nan(nsubj,2); % choice repetition bias
betab_fit = nan(nsubj,2); % bound strength (defined only if beta bound)
epsib_fit = nan(nsubj,2); % bound strength (defined only if epsi bound)

% model recovery results
betav_rec = nan(nsubj,2,nrec); % choice sensitivity to target value
betac_rec = nan(nsubj,2,nrec); % choice sensitivity to information value
betar_rec = nan(nsubj,2,nrec); % choice repetition bias
betab_rec = nan(nsubj,2,nrec); % bound strength (defined only if beta bound)
epsib_rec = nan(nsubj,2,nrec); % bound strength (defined only if epsi bound)

% model-free metrics
vopt_sub  = nan(nsubj,2,3,20); % choice direction wrt target for subjects
vopt_mod  = nan(nsubj,2,3,20); % choice direction wrt target for model simulations
copt_sub  = nan(nsubj,2,3,20); % choice direction wrt certainty for subjects
copt_mod  = nan(nsubj,2,3,20); % choice direction wrt certainty for model simulations
ndif_sub  = nan(nsubj,2,3,20); % sampling imbalance for subjects
ndif_mod  = nan(nsubj,2,3,20); % sampling imbalance for model simulations
ndif_opt  = nan(nsubj,2,3,20); % sampling imbalance for optimal decisions
chmat_sub = nan(nsubj,2,3,20,20); % choice-choice similarity matrix for subjects
chmat_mod = nan(nsubj,2,3,20,20); % choice-choice similarity matrix for model simulations
chmat_opt = nan(nsubj,2,3,20,20); % choice-choice similarity matrix for optimal decisions

if ~isempty(bound)
    % fix condition-wise bound values
    if numel(bound) ~= 2
        error('invalid size for condition-wise bound values!');
    end
    bound_fit(:,1) = bound(1);
    bound_fit(:,2) = bound(2);
end


%%% loop over participants:
for isubj = 1:nsubj
    
    % load data
    subj = subjlist(isubj);
    fprintf('processing S%02d\n',subj);
    nsq     = numel(choicestbl.seq(choicestbl.subj == subj));
    conditn = choicestbl.condition(choicestbl.subj == subj);  % condition (1:target 2:information)
    ctfactn = choicestbl.symmetry(choicestbl.subj == subj);   % 1:good/good 2:bad/bad 3:good/bad
    targetc = (2 * [choicestbl.targetcolor(choicestbl.subj == subj)] - 3) .* (conditn ~= 2); % target color (+1/-1 or 0)    
    colvals = choicestbl.coloroptions(choicestbl.subj == subj); % colors generated for each option
    choices = choicestbl.choices(choicestbl.subj == subj);      % choices of participants
    choices = cellfun( @(x) x(~isnan(x)), choices, 'UniformOutput', 0); % removing trailing NaNs
     
    for icond = 1:2
        fprintf('processing condition %d\n',icond);
        
        %%% (a) fit model:
        
        % identify condition of interest
        ifilt = conditn == icond;
        
        % create configuration structure
        cfg         = [];
        cfg.conditn = icond;
        cfg.targetc = targetc(ifilt);
        cfg.colvals = colvals(ifilt);
        cfg.choices = choices(ifilt);
        cfg.btype   = btype;

        % get best-fitting bound value based on BIC
        if isnan(bound_fit(isubj,icond))
            fprintf('  * fitting bound value\n');
            for i = 1:21
                cfg.bound = i-1;
                out = modelfit_fit_modelbound(cfg);
                aic_bound(isubj,icond,i) = out.aic;
                bic_bound(isubj,icond,i) = out.bic;
            end
            [~,i] = min(bic_bound(isubj,icond,:));
            bound_fit(isubj,icond) = i-1;
        end

        % fit parameters using best-fitting bound
        fprintf('  * fitting parameters\n');
        cfg.bound = bound_fit(isubj,icond);
        cfg.nsim  = nsim;
        out = modelfit_fit_modelbound(cfg);
        
        % get best-fitting model parameters
        betav_fit(isubj,icond) = out.betav;
        betac_fit(isubj,icond) = out.betac;
        betar_fit(isubj,icond) = out.betar;
        switch btype
            case 'beta'
                betab_fit(isubj,icond) = out.betab;
            case 'epsi'
                epsib_fit(isubj,icond) = out.epsib;
        end
        
        %%% (b) compute model-free metrics:
        fprintf('  * computing model-free metrics\n');
        
        % compute choice direction wrt target and certainty
        for ifact = 1:3
            jfilt = ctfactn(ifilt) == ifact;
            vopt_s = out.vopt(jfilt);
            copt_s = out.copt(jfilt);
            vopt_m = out.vopt_mod(jfilt);
            copt_m = out.copt_mod(jfilt);
            nseq = nnz(jfilt);
            vopt_sub_tmp = nan(nseq,20);
            copt_sub_tmp = nan(nseq,20);
            vopt_mod_tmp = nan(nseq,20);
            copt_mod_tmp = nan(nseq,20);
            for iseq = 1:nseq
                nsmp = numel(vopt_s{iseq});
                vopt_sub_tmp(iseq,1:nsmp) = vopt_s{iseq};
                copt_sub_tmp(iseq,1:nsmp) = copt_s{iseq};
                vopt_mod_tmp(iseq,1:nsmp) = mean(vopt_m{iseq},1);
                copt_mod_tmp(iseq,1:nsmp) = mean(copt_m{iseq},1);
            end
            vopt_sub_tmp(:,1) = nan;
            copt_sub_tmp(:,1) = nan;
            vopt_mod_tmp(:,1) = nan;
            copt_mod_tmp(:,1) = nan;
            vopt_sub(isubj,icond,ifact,:) = nanmean(vopt_sub_tmp,1);
            copt_sub(isubj,icond,ifact,:) = nanmean(copt_sub_tmp,1);
            vopt_mod(isubj,icond,ifact,:) = nanmean(vopt_mod_tmp,1);
            copt_mod(isubj,icond,ifact,:) = nanmean(copt_mod_tmp,1);
        end
        
        % compute sampling imbalance
        for ifact = 1:3
            jfilt = ctfactn(ifilt) == ifact;
            ndif_s = out.ndif(jfilt);
            ndif_m = out.ndif_mod(jfilt);
            ndif_o = out.ndif_opt(jfilt);
            nseq = nnz(jfilt);
            ndif_sub_tmp = nan(nseq,20);
            ndif_mod_tmp = nan(nseq,20);
            ndif_opt_tmp = nan(nseq,20);
            for iseq = 1:nseq
                nsmp = numel(ndif_s{iseq});
                ndif_sub_tmp(iseq,1:nsmp) = ndif_s{iseq};
                ndif_mod_tmp(iseq,1:nsmp) = mean(ndif_m{iseq},1);
                ndif_opt_tmp(iseq,1:nsmp) = ndif_o{iseq};
            end
            ndif_sub(isubj,icond,ifact,:) = nanmean(ndif_sub_tmp,1);
            ndif_mod(isubj,icond,ifact,:) = nanmean(ndif_mod_tmp,1);
            ndif_opt(isubj,icond,ifact,:) = nanmean(ndif_opt_tmp,1);
        end

        % compute choice-choice similarity matrix
        for ifact = 1:3
            jfilt = ctfactn(ifilt) == ifact;
            sch = cfg.choices(jfilt);
            mch = out.mch(jfilt);
            och = out.opt(jfilt);
            nseq = nnz(jfilt);
            scm = nan(nseq,20,20);
            mcm = nan(nseq,20,20);
            ocm = nan(nseq,20,20);
            for iseq = 1:nseq
                nsmp = numel(sch{iseq});
                for i1 = 1:nsmp
                    for i2 = 1:nsmp
                        scm(iseq,i1,i2) = sch{iseq}(i1) == sch{iseq}(i2);
                        mcm(iseq,i1,i2) = mean(mch{iseq}(:,i1) == mch{iseq}(:,i2),1);
                        ocm(iseq,i1,i2) = och{iseq}(i1) == och{iseq}(i2);
                    end
                end
            end
            chmat_sub(isubj,icond,ifact,:,:) = nanmean(scm,1);
            chmat_mod(isubj,icond,ifact,:,:) = nanmean(mcm,1);
            chmat_opt(isubj,icond,ifact,:,:) = nanmean(ocm,1);
        end
        
        %%% (c) perform model recovery tests
        if ~isempty(nrec) && nrec > 0
            fprintf('  * performing model recovery tests\n');
            mch = out.mch;
            for irec = 1:nrec
                % fit simulated choices
                cfg.choices = cellfun(@(c)c(irec,:),mch,'UniformOutput',false);
                cfg.nsim = [];
                out = modelfit_fit_modelbound(cfg);
                % get best-fitting model parameters
                betav_rec(isubj,icond,irec) = out.betav;
                betac_rec(isubj,icond,irec) = out.betac;
                betar_rec(isubj,icond,irec) = out.betar;
                switch btype
                    case 'beta'
                        betab_rec(isubj,icond,irec) = out.betab;
                    case 'epsi'
                        epsib_rec(isubj,icond,irec) = out.epsib;
                end
            end
        end
        
    end
end

%% save results to save time

if isempty(bound), bcalc = 'fit'; else, bcalc = 'fix'; end
filename = sprintf('dat_modelbound_bound%s_%s.mat',bcalc,btype);
save(filename, ...
    'subjlist','bound','btype','nsim','nrec', ...
    'aic_bound','bic_bound','bound_fit', ...
    'betav_fit','betac_fit','betar_fit','betab_fit','epsib_fit', ...
    'betav_rec','betac_rec','betar_rec','betab_rec','epsib_rec', ...
    'vopt_sub','vopt_mod','copt_sub','copt_mod', ...
    'ndif_sub','ndif_mod','ndif_opt', ...
    'chmat_sub','chmat_mod','chmat_opt');
fprintf('saved results to %s.\n',filename);

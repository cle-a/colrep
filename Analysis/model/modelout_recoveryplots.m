%%% plot model recovery results
%%% load a dat_modelbound_*.mat and plots the model recovery results
%%% actual recovery computations are done in the 

close all
clc

load('dat_modelbound_boundfix_beta.mat') % specify which model to choose 
load('../../../COLEXP/Analysis/Valentin/dat_modelbound_boundfix_epsi.mat')

nsubj = size(betav_fit,1);
nrec = size(betav_rec,3);

for irec = 1:nrec
    r2_betav(irec) = corr(betav_fit(:),reshape(betav_rec(:,:,irec),[],1))^2;
    r2_betac(irec) = corr(betac_fit(:),reshape(betac_rec(:,:,irec),[],1))^2;
    r2_betar(irec) = corr(betar_fit(:),reshape(betar_rec(:,:,irec),[],1))^2;
    r2_epsib(irec) = corr(epsib_fit(:),reshape(epsib_rec(:,:,irec),[],1))^2;
end

%%
% choice sensitivity to target value
xf = betav_fit;
xr = mean(betav_rec,3); sr = 1.96*std(betav_rec,[],3);
pbar = 1;
figure('Color','white');
rgb = colormap('Lines');
rgb = rgb([2,1],:);
hold on
xlim([-0.5,+6.5]);
ylim([-0.5,+6.5]);
plot(xlim,ylim,'-','Color',[0.5,0.5,0.5]);
plot(xlim,[0,0],'k-');
plot([0,0],ylim,'k-');
for i = 1:nsubj
    plot(xf(i)*[1,1],xr(i)+sr(i)*[-1,+1],'-','Color',0.5*(rgb(1,:)+1));
end
plot(xf(:,1),xr(:,1),'ko','MarkerSize',5,'MarkerFaceColor',0.5*(1+rgb(1,:)),'Color',rgb(1,:));
hold off
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',0:2:6,'YTick',0:2:6);
xlabel('simulated parameter','FontSize',8);
ylabel('recovered parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

% choice sensitivity to information value
xf = betac_fit;
xr = mean(betac_rec,3); sr = 1.96*std(betac_rec,[],3);
pbar = 1;
figure('Color','white');
rgb = colormap('Lines');
rgb = rgb([2,1],:);
hold on
xlim([-1.2,+0.6]);
ylim([-1.2,+0.6]);
plot(xlim,ylim,'-','Color',[0.5,0.5,0.5]);
plot(xlim,[0,0],'k-');
plot([0,0],ylim,'k-');
for isubj = 1:nsubj
    for i = 1:2
        plot(xf(isubj,i)*[1,1],xr(isubj,i)+sr(isubj,i)*[-1,+1],'-','Color',0.5*(1+rgb(i,:)));
    end
end
for i = 1:2
    plot(xf(:,i),xr(:,i),'ko','MarkerSize',5,'MarkerFaceColor',0.5*(1+rgb(i,:)),'Color',rgb(i,:));
end
hold off
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',-1:0.5:+1,'YTick',-1:0.5:+1);
xlabel('simulated parameter','FontSize',8);
ylabel('recovered parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

% choice repetition bias
xf = betar_fit;
xr = mean(betar_rec,3); sr = 1.96*std(betar_rec,[],3);
pbar = 1;
figure('Color','white');
rgb = colormap('Lines');
rgb = rgb([2,1],:);
hold on
xlim([-0.5,+2.5]);
ylim([-0.5,+2.5]);
plot(xlim,ylim,'-','Color',[0.5,0.5,0.5]);
plot(xlim,[0,0],'k-');
plot([0,0],ylim,'k-');
for isubj = 1:nsubj
    for i = 1:2
        plot(xf(isubj,i)*[1,1],xr(isubj,i)+sr(isubj,i)*[-1,+1],'-','Color',0.5*(1+rgb(i,:)));
    end
end
for i = 1:2
    plot(xf(:,i),xr(:,i),'ko','MarkerSize',5,'MarkerFaceColor',0.5*(1+rgb(i,:)),'Color',rgb(i,:));
end
hold off
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',0:1:2,'YTick',0:1:2);
xlabel('simulated parameter','FontSize',8);
ylabel('recovered parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

% bound strength
xf = epsib_fit;
xr = median(epsib_rec,3); sr = 1.96*std(epsib_rec,[],3);
pbar = 1;
figure('Color','white');
rgb = colormap('Lines');
rgb = rgb([2,1],:);
hold on
xlim([0,1]);
ylim([0,1]);
plot(xlim,ylim,'-','Color',[0.5,0.5,0.5]);
for isubj = 1:nsubj
    for i = 1:2
        plot(xf(isubj,i)*[1,1],xr(isubj,i)+sr(isubj,i)*[-1,+1],'-','Color',0.5*(1+rgb(i,:)));
    end
end
for i = 1:2
    plot(xf(:,i),xr(:,i),'ko','MarkerSize',5,'MarkerFaceColor',0.5*(1+rgb(i,:)),'Color',rgb(i,:));
end
hold off
set(gca,'Layer','bottom','Box','off','PlotBoxAspectRatio',[pbar,1,1]);
set(gca,'TickDir','out','TickLength',[1,1]*0.02/max(pbar,1));
set(gca,'FontName','Helvetica','FontSize',7.2);
set(gca,'XTick',0:0.5:1,'YTick',0:0.5:1);
xlabel('simulated parameter','FontSize',8);
ylabel('recovered parameter','FontSize',8);
axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes),
    if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
    if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end

function [hdl] = fx_drawcurve(Y, COLOR, PATCH)
%%% takes: -  Y the matrix to plot
%%%        - PATCH the type of interval to plot: either SEM or CI
%%%        - COLOR the color of the curve and patch
%%% plots the curve, learning-curve-style, no smoothing

    x = 1:size(Y,2);
    meany = nanmean(Y, 1);
    serry = nanstd(Y, 0, 1) / sqrt(size(Y, 1));
    if strcmp(PATCH, 'ci')
        cv = 1.96; % critical value for CI 95%
        ciup = sum([meany; serry*cv], 'omitnan');
        cilo = sum([meany; -serry*cv], 'omitnan');
    elseif strcmp(PATCH, 'sem')
        ciup = sum([meany; serry], 'omitnan');
        cilo = sum([meany; -serry], 'omitnan');
    else
        warning('default patch is confidence interval')
        cv = 1.96; % critical value for CI 95%
        ciup = sum([meany; serry*cv], 'omitnan');
        cilo = sum([meany; -serry*cv], 'omitnan');
    end

    % plot line and patch interval
    l = plot(x, meany, '-', 'Color', COLOR, 'LineWidth', 1);
    set(l, 'HandleVisibility','off');
    hold on
    x_ax     = x;
    X_patch  = [x_ax, fliplr(x_ax)];
    Y_patch  = [cilo fliplr(ciup)];
    h = fill(X_patch, Y_patch , 1,...
        'facecolor', COLOR, ...
        'edgecolor','none', ...
        'facealpha', 0.3);
    set(h, 'HandleVisibility','on');

    % prettying
    set(gca,'PlotBoxAspectRatio', [1,1,1])
    set(gca,'Box','off','TickDir','out');
    set(gca,'FontName','Helvetica','FontSize', 12);
    axes = findobj(gcf, 'type', 'axes');
    for a = 1:length(axes)
        if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
        if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
    end

    hdl = gcf;
end
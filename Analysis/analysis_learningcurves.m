%%% check learning in condition 1 good/bad sequences = learning curve & proportion of correct final sampling choice
%%% draws the average learning curve over participants, overall and per length
%%% draws the distribution of correct final samples,  

clearvars;

lcol = lines;
col(1,:) = lcol(4,:);
col(2,:) = lcol(5,:);

load('choicestbl.mat');
choicestbl = choicestbl(choicestbl.excludepart == 0,:);
subj       = unique(choicestbl.subj);
nsubj      = numel(subj);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% draw learning curves: subset condition 1, good/bad sequences

% average correctness for each choice whithin subj
fprintf('collecting learning x sequence lengths\n');
lengths = unique(choicestbl.nsamples);
learningmat = nan(numel(subj), 20, numel(lengths));
finalsamplesmat = nan(numel(subj), numel(lengths));
for isubj = 1:nsubj
    subjn = subj(isubj);
    for ilen = 1:numel(lengths)
        subset  = choicestbl(choicestbl.subj == subjn & choicestbl.counterfactual == 1 & choicestbl.nsamples == lengths(ilen),:);
        choices = cell2mat(subset.choices) * -1 + 2; % choices, converted from 1/2 to 1/0
        learningmat(isubj, :, ilen)     = nanmean(choices, 1); % averaging of all choices
        finalsamplesmat(isubj, ilen)    = nanmean(choices(:,lengths(ilen)));
    end
end% for each subj

%%% draw the curves %%%
fprintf('drawing learning curves\n');
figure('Color','white','units','centimeters', 'position', [10 10 10 10]);

fx_drawcurve(nanmean(learningmat,3), col(1,:), 'ci');
l = plot([0 20], [0.5 0.5], 'LineStyle', '--', 'Color', [0.5 0.5 0.5]);

set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTick', [1,8,12,16,20], 'FontName', 'Helvetica', 'FontSize',7.2);
pbar = 1;
set(gca, 'Layer', 'bottom', 'Box', 'off', 'PlotBoxAspectRatio', [pbar,1,1]);
set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));

xlabel('choice position in sequence', 'FontSize', 8);
ylabel('ratio of target-symbol choices', 'FontSize', 8);
title('"learning curve" in condition 1, good/bad sequences', 'Fontweight', 'normal', 'FontSize', 8)


%%% draw the curve per lengths %%%
figure('Color', [1 1 1],'units','centimeters','position',[10 10 10 10])
for ilen = 1:numel(lengths)
    fx_drawcurve(learningmat(:,:,ilen), lcol(ilen+6,:), 'sem');
    hold on
end
l = plot([0 20], [0.5 0.5], 'LineStyle', '--', 'Color', [0.5 0.5 0.5]);

set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTick', [1,8,12,16,20], 'FontName', 'Helvetica', 'FontSize',7.2);
pbar = 1;
set(gca, 'Layer', 'bottom', 'Box', 'off', 'PlotBoxAspectRatio', [pbar,1,1]);
set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));

legend('8','12','16','20', 'Location', 'SouthEast')
xlabel('choice position in sequence', 'FontSize', 8);
ylabel('ratio of target-symbol choices', 'FontSize', 8);
title(sprintf('"learning curve" in condition 1, good/bad sequences\n per sequence length'), 'Fontweight', 'normal', 'FontSize', 8)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% draw distribution of the direction of final sampling choices:
fprintf('plotting violins for final sampling choices\n');
figure('Color', [1 1 1],'units','centimeters','position',[10 10 10 10])

violins = violinplot(nanmean(finalsamplesmat,2), 1, 'Width', 0.2); % averaging on the seqlength dimension and keeping only col 3 and 4 (diff and avg same)
violins.ViolinColor = 0.5 * (col(2,:) + 1);
violins.EdgeColor   = 'none';
violins.ViolinAlpha = 0.3;
violins.ScatterPlot.SizeData = 70;
violins.ScatterPlot.MarkerFaceColor = col(2,:);
violins.ScatterPlot.MarkerFaceAlpha = 0.8;

%add chance
thr = 0.5;
xl = [0.5 1.5]; xlim(xl);
plot(linspace(xl(1),xl(2)), repelem(thr, 100), 'LineStyle', '--', 'Color', [0.5 0.5 0.5], 'LineWidth', 0.5) 

set(gca, 'YLim', [0 1], 'YTick', [0:0.2:1], 'XTickLabel', {''}, 'FontName', 'Helvetica', 'FontSize',7.2);
pbar = 1;
set(gca, 'Layer', 'bottom', 'Box', 'off', 'PlotBoxAspectRatio', [pbar,1,1]);
set(gca, 'TickDir', 'out', 'TickLength', [1,1]*0.02/max(pbar,1));

axes = findobj(gcf, 'type', 'axes');
for a = 1:length(axes); if axes(a).YColor <= [1 1 1]; axes(a).YColor = [0 0 0]; end
                        if axes(a).XColor <= [1 1 1]; axes(a).XColor = [0 0 0]; end
end
pbar = 1;
set(gca,'box','off','TickDir', 'out');

% labels
xlabel('condition 1', 'FontSize', 8);
ylabel('correct responses', 'FontSize', 8);
title(sprintf('ratio of correct responses at final *sampling choice*\nin condition 1, good/bad sequences'), 'Fontweight', 'normal', 'FontSize', 8);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% test whether sample indeed responds above chance across lengths:
[h, p, ci] = ttest(nanmean(finalsamplesmat,2), 0.5) % 1 rejects H0 that sample is no different to 50





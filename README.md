This repository contains the experiment code, data, and analysis code for the *replication dataset* described in _Competing cognitive pressures on human exploration in the absence of trade-off with exploitation_ -Alméras, Chambon, Wyart.


- Experiment/ : contains code used to generate the task and display it to participants.  
This is based on Pschychtoolbox-3 (we ran 3.0.14 on MATLAB 2017b): http://psychtoolbox.org/

- Data/ : contains the task structure, and participants responses.

- Analysis/ : contains scripts of the analysis pipeline.  
The model fitting code uses the fmincon function, from MATLAB's Optimization Toolbox: https://www.mathworks.com/products/optimization.html  
The model comparison code uses the spm_BMS function, from the SPM toolbox (we used spm12): https://www.fil.ion.ucl.ac.uk/spm/software/download/  

**Note**: The experiment code, task structures and analysis scripts were uploaded before acquisition. Data was added afterwards.
The other datasets and the final version of the pipeline adapted to process all three datasets described in the manuscript are available at https://gitlab.com/cle-a/colpub
